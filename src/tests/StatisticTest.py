from unittest import TestCase

from src.controller.Statistic import Statistics


class TestTest(TestCase):
    def setUp(self):
        self.statistics.clearStatisticInfo()

    statistics = Statistics()

    # actual Testcase
    def test_add_statistic_info(self):
        self.statistics.addStatisticInfo("test text")

        self.assertEqual(self.statistics.statisticinfo, "test text\n" )

    def test_add_multiple_statistic_info(self):
        self.statistics.addStatisticInfo("test text")
        self.statistics.addStatisticInfo("test text2")
        self.statistics.addStatisticInfo("test text3")

        self.assertEqual(self.statistics.statisticinfo, "test text\ntest text2\ntest text3\n" )

    def test_add_statistic_info_before_and_after_clear(self):
        self.statistics.addStatisticInfo("test before")
        self.statistics.clearStatisticInfo()
        self.statistics.addStatisticInfo("test after")

        self.assertEqual(self.statistics.statisticinfo, "test after\n" )

    def test_get_statistic_info_after_add_multiple_statistic_info(self):
        self.statistics.addStatisticInfo("test text")
        self.statistics.addStatisticInfo("test text2")
        self.statistics.addStatisticInfo("test text3")

        self.assertEqual(self.statistics.getStatisticInfo(), "test text\ntest text2\ntest text3\n" )

    def test_get_statistic_info_after_add_statistic_info_before_and_after_clear(self):
        self.statistics.addStatisticInfo("test before")
        self.statistics.clearStatisticInfo()
        self.statistics.addStatisticInfo("test after")

        self.assertEqual(self.statistics.getStatisticInfo(), "test after\n" )