from unittest import TestCase
from src.controller.TransformToXml import TransformToXml
from src.model.Product import Product
from src.model.Review import Review
import xml.etree.ElementTree as ET
import os


class TransformToXmlTest(TestCase):
    transformToXml = TransformToXml()

    review_one = Review(["powolny", "mała pojemność"], ["cena", "wyglad"], "Polecam", 4, "Killer", "12.12.2015", "True",
                        1, 2, "Ceneo")
    review_two = Review(["powolny"], ["cena"], "Polecam", 3, "Nowak", "06.12.2015", "True", 0, 1, "Ceneo")
    review_three = Review(["powolny", "mała pojemność"], [], "Zdecydowanie nie polecam", 1, "Killer", "12.12.2015",
                          "False", 0, 0, "Ceneo")
    review_four = Review(["bardzo powolny", "bardzo mała pojemność"], [], "Nie polecam", 1, "Killero", "12.18.2015",
                         "False", 6, 10, "Skapiec")

    product_one = Product("26380453", "", "Dysk SSD", "ADATA", "S102 PRO 256GB", "Super szybki dysk SSD",
                          [review_one, review_two])
    product_two = Product("36380453", "/site/cat/56/comp/718280", "Dysk HDD", "WD", "2TB", "Dysk HDD", [review_three, review_four])

    two_products = [product_one, product_two]

    def tearDown(self):
        tmp_files = {'Ceneo_first_product.xml', 'Skapiec_first_product.xml', 'Ceneo_second_product.xml',
                     'Skapiec_second_product.xml', 'products.xml'}
        for file in tmp_files:
            os.remove(file)

    def Dummy_increase_progress(self, dumytext, steep):
         pass

    def Dummy_progress_completed(self):
         pass

    def test_transform_two_product(self):
        product_string = '<ETL_Products>' \
                            '<product_code id_ceneo="26380453" id_skapiec="">' \
                                '<type_of_device>Dysk SSD</type_of_device>' \
                                '<product_brand>ADATA</product_brand>' \
                                '<model_name>S102 PRO 256GB</model_name>' \
                                '<extra_information>Super szybki dysk SSD</extra_information>'\
                            '</product_code>' \
                            '<product_code id_ceneo="36380453" id_skapiec="/site/cat/56/comp/718280">' \
                                '<type_of_device>Dysk HDD</type_of_device>' \
                                '<product_brand>WD</product_brand>' \
                                '<model_name>2TB</model_name>' \
                                '<extra_information>Dysk HDD</extra_information>'\
                            '</product_code>' \
                         '</ETL_Products>'

        Ceneo_first_product = '<ETL_Ceneo>' \
                                '<review id="1">' \
                                    '<defect id="1">powolny</defect>' \
                                    '<defect id="2">mała pojemność</defect>' \
                                    '<advantage id="1">cena</advantage>' \
                                    '<advantage id="2">wyglad</advantage>' \
                                    '<content_of_review>Polecam</content_of_review>' \
                                    '<number_of_stars>4</number_of_stars>' \
                                    '<review_author>Killer</review_author>' \
                                    '<review_date>12.12.2015</review_date>' \
                                    '<is_recommend>True</is_recommend>' \
                                    '<number_of_positive_votes>1</number_of_positive_votes>' \
                                    '<number_of_all_votes>2</number_of_all_votes>' \
                                    '<service_name>Ceneo</service_name>' \
                                '</review>' \
                                '<review id="2">' \
                                    '<defect id="1">powolny</defect>' \
                                    '<advantage id="1">cena</advantage>' \
                                    '<content_of_review>Polecam</content_of_review>' \
                                    '<number_of_stars>3</number_of_stars>' \
                                    '<review_author>Nowak</review_author>' \
                                    '<review_date>06.12.2015</review_date>' \
                                    '<is_recommend>True</is_recommend>' \
                                    '<number_of_positive_votes>0</number_of_positive_votes>' \
                                    '<number_of_all_votes>1</number_of_all_votes>' \
                                    '<service_name>Ceneo</service_name>' \
                                '</review>' \
                              '</ETL_Ceneo>'

        Skapiec_first_product = '<ETL_Skapiec />'

        Ceneo_second_product = '<ETL_Ceneo>' \
                                '<review id="1">' \
                                    '<defect id="1">powolny</defect>' \
                                    '<defect id="2">mała pojemność</defect>' \
                                    '<content_of_review>Zdecydowanie nie polecam</content_of_review>' \
                                    '<number_of_stars>1</number_of_stars>' \
                                    '<review_author>Killer</review_author>' \
                                    '<review_date>12.12.2015</review_date>' \
                                    '<is_recommend>False</is_recommend>' \
                                    '<number_of_positive_votes>0</number_of_positive_votes>' \
                                    '<number_of_all_votes>0</number_of_all_votes>' \
                                    '<service_name>Ceneo</service_name>' \
                                '</review>' \
                               '</ETL_Ceneo>'

        Skapiec_second_product = '<ETL_Skapiec>' \
                                    '<review id="1">' \
                                        '<defect id="1">bardzo powolny</defect>' \
                                        '<defect id="2">bardzo mała pojemność</defect>' \
                                        '<content_of_review>Nie polecam</content_of_review>' \
                                        '<number_of_stars>1</number_of_stars>' \
                                        '<review_author>Killero</review_author>' \
                                        '<review_date>12.18.2015</review_date>' \
                                        '<is_recommend>False</is_recommend>' \
                                        '<number_of_positive_votes>6</number_of_positive_votes>' \
                                        '<number_of_all_votes>10</number_of_all_votes>' \
                                        '<service_name>Skapiec</service_name>' \
                                    '</review>' \
                                '</ETL_Skapiec>'


        xml_list_holder = self.transformToXml.transform(self.two_products, self.Dummy_increase_progress, self.Dummy_progress_completed)

        ET.ElementTree(xml_list_holder[0]).write("Ceneo_first_product.xml", encoding="unicode")
        ET.ElementTree(xml_list_holder[1]).write("Skapiec_first_product.xml", encoding="unicode")
        ET.ElementTree(xml_list_holder[2]).write("Ceneo_second_product.xml", encoding="unicode")
        ET.ElementTree(xml_list_holder[3]).write("Skapiec_second_product.xml", encoding="unicode")
        ET.ElementTree(xml_list_holder[4]).write("products.xml", encoding="unicode")

        output_Ceneo_first_product = open('Ceneo_first_product.xml').read()
        output_Skapiec_first_product = open('Skapiec_first_product.xml').read()
        output_Ceneo_second_product = open('Ceneo_second_product.xml').read()
        output_Skapiec_second_product = open('Skapiec_second_product.xml').read()
        output_product_string = open('products.xml').read()

        self.assertEqual(Ceneo_first_product, output_Ceneo_first_product)
        self.assertEqual(Skapiec_first_product, output_Skapiec_first_product)
        self.assertEqual(Ceneo_second_product, output_Ceneo_second_product)
        self.assertEqual(Skapiec_second_product, output_Skapiec_second_product)
        self.assertEqual(product_string, output_product_string)
