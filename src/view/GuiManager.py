import atexit
import sys
import os
import time
from PyQt4 import QtCore, QtGui

from src.controller import Statistic
from src.controller.ProductCode import get_product_code
from src.controller.HtmlParser import HtmlParser
from src.controller.TransformToXml import TransformToXml
from src.controller.LoaderToXmlDataBase import LoaderToXmlDataBase
from src.controller.ExportToCsv import ExportToCSV
from src.view import MainWindow
from src.view import FeedbackWindow
from src.view import SelectProductWindow
from src.view import ShowDb
from src.view import ShowDbRecords
from src.view.ShowDbManager import load_product, load_product_from_db, filter_product, read_review_from_db, load_review
from src.controller.ManageProductsXmlInDb import remove_product_in_db, remove_ceneo_review, remove_skapiec_review, \
    remove_all_db
from urllib.error import URLError, HTTPError


class Feedback(QtGui.QMainWindow, FeedbackWindow.Ui_FeedbackWindow):
    statistics = Statistic.Statistics()

    def __init__(self, parent=None):
        super(Feedback, self).__init__(parent)
        self.setupUi(self)
        self.closeButton.clicked.connect(self.close)
        self.feedbackTextBrowser

    def setFeedbackText(self, text):
        self.feedbackTextBrowser.setText(text)


class SelectProduct(QtGui.QDialog, SelectProductWindow.Ui_SelectProductWindow):
    def __init__(self, parent=None):
        super(SelectProduct, self).__init__(parent)
        self.dialog = QtGui.QDialog(parent)
        self.setupUi(self.dialog)
        # QtGui.QWidget.__init__(self)
        self.setWindowFlags(QtCore.Qt.Dialog)
        self.setupUi(self)
        #
        # super(SelectProduct, self).__init__(parent)
        # self.setupUi(self)
        # self.SelectedProductBtn.clicked.connect(self.accept)  #Clicked accept


class DataBaseWindow(QtGui.QMainWindow, ShowDb.Ui_ShowDb):
    statistics = Statistic.Statistics()

    def __init__(self, parent=None):
        super(DataBaseWindow, self).__init__(parent)
        self.dialogDataBase = QtGui.QDialog(parent)
        self.setupUi(self.dialogDataBase)
        self.setWindowFlags(QtCore.Qt.Dialog)
        self.setupUi(self)


class DataBaseReviewWindow(QtGui.QMainWindow, ShowDbRecords.Ui_ShowDbRecords):
    statistics = Statistic.Statistics()

    def __init__(self, parent=None):
        super(DataBaseReviewWindow, self).__init__(parent)
        self.dialogDataBase = QtGui.QDialog(parent)
        self.setupUi(self.dialogDataBase)
        self.setWindowFlags(QtCore.Qt.Dialog)
        self.setupUi(self)


class ETLApp(QtGui.QMainWindow, MainWindow.Ui_MainWindow):
    statistics = Statistic.Statistics()
    product_list = []
    xml_list_holder = []
    product_list_from_db = []
    product_list_from_db_backup = []
    ceneo_review_list_from_db = []
    skapiec_review_list_from_db = []

    def __init__(self, parent=None):
        super(ETLApp, self).__init__(parent)
        self.product_code_list = []
        self.setupUi(self)
        # setup Gui connection
        self.extractButton.clicked.connect(self.extract)
        self.transformButton.clicked.connect(self.transform)
        self.loadButton.clicked.connect(self.load)
        self.ETLButton.clicked.connect(self.etl)
        self.actionToTxtFile.connect(self.actionToTxtFile, QtCore.SIGNAL("triggered()"), self.exportToTxt)
        self.actionToCsvFile.connect(self.actionToCsvFile, QtCore.SIGNAL("triggered()"), self.exportToCsv)
        self.actionWyczy_baz_Ceneo.connect(self.actionWyczy_baz_Ceneo, QtCore.SIGNAL("triggered()"),
                                           self.clear_ceneo_db)
        self.actionWyczy_baz_Skapiec.connect(self.actionWyczy_baz_Skapiec, QtCore.SIGNAL("triggered()"),
                                             self.clear_skapiec_db)
        self.actionWyczy_ca_baz_danych.connect(self.actionWyczy_ca_baz_danych, QtCore.SIGNAL("triggered()"),
                                               self.clear_db)
        self.actionExit.connect(self.actionExit, QtCore.SIGNAL("triggered()"), self.exit)
        self.actionOtw_rz_Baz_danych.connect(self.actionOtw_rz_Baz_danych, QtCore.SIGNAL("triggered()"), self.openDb)
        self.feedbackDialog = None
        self.SelectProductDialog = None
        self.showDbDialog = None
        self.showDbReviewDialog = None

    # Gui Action Functions
    def showDb(self):
        if self.showDbDialog is None:
            self.showDbDialog = DataBaseWindow(self)
        self.showDbDialog.textFilterInput.connect(self.showDbDialog.textFilterInput, QtCore.SIGNAL("textChanged()"),
                                                  self.activate_product_filter)

        self.product_list_from_db = []
        load_product_from_db(self.product_list_from_db)
        self.product_list_from_db_backup = list(self.product_list_from_db)
        self.showDbDialog.show()
        load_product(self.showDbDialog.tableWidget, self.product_list_from_db)
        self.fix_table_size_for_show_db()
        self.showDbDialog.show()
        self.showDbDialog.tableWidget.connect(self.showDbDialog.tableWidget,
                                              QtCore.SIGNAL("doubleClicked(QModelIndex)"), self.showDbReview)

    def activate_product_filter(self):
        self.product_list_from_db = filter_product(self.product_list_from_db, self.product_list_from_db_backup,
                                                   self.showDbDialog.textFilterInput.toPlainText())
        load_product(self.showDbDialog.tableWidget, self.product_list_from_db)
        self.fix_table_size_for_show_db()
        self.showDbDialog.show()

    def fix_table_size_for_show_db(self):
        self.showDbDialog.tableWidget.resizeRowsToContents()
        self.showDbDialog.tableWidget.resizeColumnToContents(0)
        self.showDbDialog.tableWidget.resizeColumnToContents(1)
        self.showDbDialog.tableWidget.resizeColumnToContents(2)
        self.showDbDialog.tableWidget.resizeColumnToContents(3)

    def showDbReview(self):
        self.ceneo_review_list_from_db = []
        self.skapiec_review_list_from_db = []
        if self.showDbReviewDialog is None:
            self.showDbReviewDialog = DataBaseReviewWindow(self)
        read_review_from_db(self.ceneo_review_list_from_db,
                            self.showDbDialog.tableWidget.item(self.showDbDialog.tableWidget.currentRow(), 0).text(),
                            True)
        read_review_from_db(self.skapiec_review_list_from_db,
                            self.showDbDialog.tableWidget.item(self.showDbDialog.tableWidget.currentRow(), 1).text(),
                            False)
        load_review(self.showDbReviewDialog.tableWidgetCeneo, self.showDbReviewDialog.tableWidgetSkapiec,
                    self.showDbReviewDialog.tableWidgetAll, self.ceneo_review_list_from_db,
                    self.skapiec_review_list_from_db)
        self.fix_table_size_for_show_records()
        self.showDbReviewDialog.show()

    def fix_table_size_for_show_records(self):
        self.showDbReviewDialog.tableWidgetCeneo.resizeRowsToContents()
        self.showDbReviewDialog.tableWidgetSkapiec.resizeRowsToContents()
        self.showDbReviewDialog.tableWidgetAll.resizeRowsToContents()
        self.showDbReviewDialog.tableWidgetCeneo.resizeColumnToContents(3)
        self.showDbReviewDialog.tableWidgetCeneo.resizeColumnToContents(4)
        self.showDbReviewDialog.tableWidgetCeneo.resizeColumnToContents(5)
        self.showDbReviewDialog.tableWidgetCeneo.resizeColumnToContents(6)
        self.showDbReviewDialog.tableWidgetCeneo.resizeColumnToContents(7)
        self.showDbReviewDialog.tableWidgetCeneo.resizeColumnToContents(8)
        self.showDbReviewDialog.tableWidgetSkapiec.resizeColumnToContents(3)
        self.showDbReviewDialog.tableWidgetSkapiec.resizeColumnToContents(4)
        self.showDbReviewDialog.tableWidgetSkapiec.resizeColumnToContents(5)
        self.showDbReviewDialog.tableWidgetSkapiec.resizeColumnToContents(6)
        self.showDbReviewDialog.tableWidgetSkapiec.resizeColumnToContents(7)
        self.showDbReviewDialog.tableWidgetSkapiec.resizeColumnToContents(8)
        self.showDbReviewDialog.tableWidgetAll.resizeColumnToContents(3)
        self.showDbReviewDialog.tableWidgetAll.resizeColumnToContents(4)
        self.showDbReviewDialog.tableWidgetAll.resizeColumnToContents(5)
        self.showDbReviewDialog.tableWidgetAll.resizeColumnToContents(6)
        self.showDbReviewDialog.tableWidgetAll.resizeColumnToContents(7)
        self.showDbReviewDialog.tableWidgetAll.resizeColumnToContents(8)

    def showFeedback(self, text=None):
        if self.feedbackDialog is None:
            self.feedbackDialog = Feedback(self)
        if text is None:
            text = self.statistics.getStatisticInfo()
        self.feedbackDialog.setFeedbackText(text)
        self.feedbackDialog.show()

    def getSelectProduct(self, product_dict, text="Wybierz jeden produkt z listy znalezionych:"):
        if not product_dict:
            return
        if self.SelectProductDialog is None:
            self.SelectProductDialog = SelectProduct(self)
        self.SelectProductDialog.SelectedProductCombo.clear()
        for product in product_dict.values():
            self.SelectProductDialog.SelectedProductCombo.addItem(product)
        self.SelectProductDialog.SelectedProductLbl.setText(text)
        if self.SelectProductDialog.exec_() == QtGui.QDialog.Accepted:
            return self.get_product_code_from_dict_by_value(self.SelectProductDialog.getSelected(), product_dict)

    @staticmethod
    def get_product_code_from_dict_by_value(product, product_dict):
        for key, val in product_dict.items():
            if val == product:
                return key
        return None

    def extract(self):
        if not self.ProductText.toPlainText():
            self.feedbackError("Error. Nie Podano listy produktów")
            return
        self.set_disable_enable_checkbox(True)
        self.ETLButton.setDisabled(True)
        self.statistics.clearStatistic()
        start_time = time.clock()
        self.set_product_code_list()
        self.get_products_from_web()
        self.statistics.time_extract = str(time.clock() - start_time)
        self.__refresh()
        self.showFeedback(self.statistics.getStatistic())

    def set_product_code_list(self):
        self.product_code_list = get_product_code(self.radioButtonCeneo.isChecked(), self.ProductText.toPlainText(),
                                                  self.statistics)

    def set_disable_enable_checkbox(self, disable):
        self.checkBoxSecondService.setDisabled(disable)
        self.radioButtonCeneo.setDisabled(disable)
        self.radioButtonSkapiec.setDisabled(disable)

    def transform(self):
        if not self.product_list:
            self.feedbackError("Error. Nie wykonano operacj Extract")
            return
        self.statistics.clearStatistic()
        start_time = time.clock()
        self.set_number_of_steeps(self.calculate_trensform_steeps())
        transform_to_xml = TransformToXml()
        self.xml_list_holder = transform_to_xml.transform(self.product_list, self.increase_progress,
                                                          self.progress_completed,
                                                          self.statistics.increase_products_processed_transform,
                                                          self.statistics.increase_review_processed_transform)
        self.statistics.set_time_transform(str(time.clock() - start_time))
        self.showFeedback(self.statistics.getStatistic())
        self.product_list = []

    def load(self):
        if not self.xml_list_holder:
            self.feedbackError("Error. Nie wykonano operacj Transform")
            return
        self.statistics.clearStatistic()
        start_time = time.clock()
        self.set_number_of_steeps(self.calculate_trensform_steeps())
        loaderToXmlDataBase = LoaderToXmlDataBase()
        loaderToXmlDataBase.load(self.xml_list_holder, self.increase_progress, self.progress_completed,
                                 self.statistics.increase_products_updated_load,
                                 self.statistics.increase_products_new_load)
        self.statistics.set_time_load(str(time.clock() - start_time))
        self.showFeedback(self.statistics.getStatistic())
        self.set_disable_enable_checkbox(False)
        self.ETLButton.setDisabled(False)
        self.xml_list_holder = []

    def etl(self):
        if not self.ProductText.toPlainText():
            self.feedbackError("Error. Nie Podano listy produktów")
            return
        self.statistics.clearStatistic()
        self.set_disable_enable_buttons(True)
        self.extract()

        start_time = time.clock()
        self.set_number_of_steeps(self.calculate_trensform_steeps())
        transform_to_xml = TransformToXml()
        self.xml_list_holder = transform_to_xml.transform(self.product_list, self.increase_progress,
                                                          self.progress_completed,
                                                          self.statistics.increase_products_processed_transform,
                                                          self.statistics.increase_review_processed_transform)
        self.statistics.set_time_transform(str(time.clock() - start_time))
        self.product_list = []

        start_time = time.clock()
        self.set_number_of_steeps(self.calculate_load_steeps())
        loader_to_xml_data_base = LoaderToXmlDataBase()
        loader_to_xml_data_base.load(self.xml_list_holder, self.increase_progress, self.progress_completed,
                                     self.statistics.increase_products_updated_load,
                                     self.statistics.increase_products_new_load)
        self.statistics.set_time_load(str(time.clock() - start_time))
        self.showFeedback(self.statistics.getStatistic())
        self.set_disable_enable_buttons(False)
        self.xml_list_holder = []

    def set_disable_enable_buttons(self, disable):
        self.extractButton.setDisabled(disable)
        self.transformButton.setDisabled(disable)
        self.loadButton.setDisabled(disable)

    def __refresh(self):
        self.set_disable_enable_buttons(False)
        self.set_disable_enable_checkbox(False)
        self.ETLButton.setDisabled(False)

    def get_products_from_web(self):
        for product_code in self.product_code_list:
            try:
                search_output_or_product = HtmlParser(product_code,
                                                      is_skapiec=self.radioButtonSkapiec.isChecked(),
                                                      is_both_services=self.checkBoxSecondService.isChecked(),
                                                      feedback_info=self.feedbackInfo,
                                                      get_select_product=self.getSelectProduct,
                                                      progress_bar=[self.set_number_of_steeps,
                                                                    self.increase_progress,
                                                                    self.progress_completed],
                                                      statistics=[self.statistics.increase_url_open_extract,
                                                                  self.statistics.increase_not_found_extract]) \
                    .get_search_output_or_product()
            except (HTTPError, ValueError):
                self.__refresh()
                self.statistics.increase_not_found_extract(product_code + " - Ceneo")
                continue
            except URLError:
                self.__refresh()
                self.statistics.addStatisticInfo("Błąd. Sprawdź połączenie internetowe"),
                return
            except Exception:
                self.statistics.addStatisticInfo("Unexpected error:" + str(sys.exc_info()[0]))
                self.__refresh()
                return

            if not isinstance(search_output_or_product, dict):
                self.product_list.append(search_output_or_product)
                self.statistics.increase_products_processed_extract()
                self.statistics.review_processed_extract = len(search_output_or_product.review_list)
            else:
                self.product_code_list.clear()
                product_tmp = None
                if len(search_output_or_product) == 1:
                    key, value = search_output_or_product.popitem()
                    product_tmp = str(key)
                if not product_tmp:
                    product_tmp = self.getSelectProduct(search_output_or_product)
                if product_tmp:
                    self.product_code_list.append(product_tmp)
                if self.product_code_list:
                    self.get_products_from_web()

    def exportToTxt(self):
        full_path_to_file = QtGui.QFileDialog.getSaveFileName(self, "Save file", "EtlDb", ".txt")
        if full_path_to_file[-4:] != ".txt":
            full_path_to_file +=".txt"

    def exportToCsv(self):
        full_path_to_file = QtGui.QFileDialog.getSaveFileName(self, "Save file", "EtlDb", ".csv")
        if full_path_to_file != "":
            if full_path_to_file[-4:] == ".csv":
                full_path_to_file = full_path_to_file[:(len(full_path_to_file) - 4 )]
            exportToCSV = ExportToCSV()
            exportToCSV.export(full_path_to_file + "_product.csv", full_path_to_file + "_review.csv")

    def clear_ceneo_db(self):
        remove_ceneo_review()
        remove_product_in_db(True)

    def clear_skapiec_db(self):
        remove_skapiec_review()
        remove_product_in_db(False)

    def clear_db(self):
        remove_all_db()

    def openDb(self):
        self.showDb()

    def exit(self):
        sys.exit()

    def feedbackInfo(self, feedback):
        self.FeedbackLabel.setStyleSheet("QLabel { color : black;}")
        self.FeedbackLabel.setText(feedback)

    def feedbackError(self, feedback):
        self.FeedbackLabel.setStyleSheet("QLabel { color : red; font : bold}")
        self.FeedbackLabel.setText(feedback)

    def feedbackSuccess(self, feedback):
        self.FeedbackLabel.setStyleSheet("QLabel { color : green}")
        self.FeedbackLabel.setText(feedback)

    # Progress Bar functions
    def set_number_of_steeps(self, number):
        self.total_number_of_steeps = number
        self.competed = 0
        self.feedbackInfo('Przetwarzanie')
        self.progressBar.setValue(0)

    def increase_progress(self, additional_info, steeps_completed=1):
        self.competed += steeps_completed
        self.progressBar.setValue((self.competed / self.total_number_of_steeps) * 100)
        if additional_info:
            self.feedbackInfo(additional_info)

    def progress_completed(self):
        self.progressBar.setValue(100)
        self.feedbackInfo("Zakończono")

    def calculate_trensform_steeps(self):
        steeps = self.product_list.__len__()
        for product in self.product_list:
            steeps += product.review_list.__len__()
        if steeps != 0:
            return steeps
        return 1

    def calculate_load_steeps(self):
        steeps = self.xml_list_holder.__len__() + self.xml_list_holder[-1].__len__()
        if steeps != 0:
            return steeps
        return 1


def main():
    app = QtGui.QApplication(sys.argv)
    form = ETLApp()
    form.show()
    atexit.register(cleanup)
    app.exec_()


def cleanup():
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for file in files:
        if file.endswith('.etl'):
            os.remove(file)


if __name__ == '__main__':
    main()
