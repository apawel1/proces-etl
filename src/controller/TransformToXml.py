import xml.etree.ElementTree as ET


class TransformToXml:
    def transform(self, product_list, increase_progress, progress_completed, increase_products_processed_transform,
                  increase_review_processed_transform):
        xml_holder = []
        root = ET.Element('ETL_Products')
        for i, product in enumerate(product_list):
            increase_progress("Transform: Product " + (i + 1).__str__() + "/" + product_list.__len__().__str__(),
                              product.review_list.__len__() + 1)
            product_code = ET.Element('product_code')
            root.append(product_code)
            product_code.set('id_ceneo', product.product_code_ceneo)
            product_code.set('id_skapiec', product.product_code_skapiec)

            self.add_parameter('type_of_device', product.type_of_device.replace(',', ''), product_code)
            self.add_parameter('product_brand', product.product_brand.replace(',', ''), product_code)
            self.add_parameter('model_name', product.model_name.replace(',', ''), product_code)
            self.add_parameter('extra_information', product.extra_information.replace(',', ''), product_code)

            review_id_Ceneo = 1
            review_id_Skapiec = 1

            root_Ceneo = ET.Element('ETL_Ceneo')
            root_Skapiec = ET.Element('ETL_Skapiec')

            for review in product.review_list:
                if review.service_name == "Ceneo":
                    review_id_Ceneo = self.add_product_review(review, review_id_Ceneo, root_Ceneo)
                else:
                    review_id_Skapiec = self.add_product_review(review, review_id_Skapiec, root_Skapiec)
                increase_review_processed_transform()

            xml_holder.append(root_Ceneo)
            xml_holder.append(root_Skapiec)
            increase_products_processed_transform()
        xml_holder.append(root)
        progress_completed()
        return xml_holder

    def add_product_review(self, review, review_id, root):
        current_review = ET.Element('review')
        root.append(current_review)
        current_review.set('id', review_id.__str__())

        self.add_parameters_with_id('defect', review.defects, current_review)
        self.add_parameters_with_id('advantage', review.advantages, current_review)
        self.add_parameter('content_of_review', review.content_of_review.replace(',', ''), current_review)
        self.add_parameter('number_of_stars', review.number_of_stars.__str__().replace(',', ''), current_review)
        self.add_parameter('review_author', review.review_author.replace(',', ''), current_review)
        self.add_parameter('review_date', review.review_date.replace(',', ''), current_review)
        self.add_parameter('is_recommend', review.is_recommend.replace(',', ''), current_review)
        self.add_parameter('number_of_positive_votes', review.number_of_positive_votes.__str__().replace(',', ''), current_review)
        self.add_parameter('number_of_all_votes', review.number_of_all_votes.__str__().replace(',', ''), current_review)
        self.add_parameter('service_name', review.service_name.replace(',', ''), current_review)
        review_id += 1
        return review_id

    def add_parameter(self, parameter_name, parameter_value, parent):
        parameter = ET.Element(parameter_name)
        parent.append(parameter)
        if parameter_value == "":
            parameter_value = " "
        parameter_value.replace(",", "")
        parameter.text = parameter_value

    def add_parameters_with_id(self, parameter_name, parameter_list, parent):
        current_id = 1
        if not parameter_list:
            parameter_list = [" "]
        parameter_list = [';'.join(parameter_list).replace("\n", " ")]
        for parameter in parameter_list:
            parameter.replace("\n", " ")
            parameter.replace(",", ";")
            current_parameter = ET.Element(parameter_name)
            parent.append(current_parameter)
            current_parameter.set('id', current_id.__str__())
            current_parameter.text = parameter
            current_id += 1
