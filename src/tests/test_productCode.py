import unittest
from src.controller.ProductCode import get_product_code
from src.controller.Statistic import Statistics


class TestTest(unittest.TestCase):
    def setUp(self):
        self.statistics.clearStatisticInfo()

    statistics = Statistics()
    separators_list = [';', ',', '\n']
    endings_list = ['', ';', ',', '\n']
    correct_product_codes_list = ['12345678', '87654321']
    correct_product_codes_list_ceneo = ['123456', '876543']
    incorrect_product_codes_list = ['12345', '876543289']
    correct_product_addresses_list = ['http://www.ceneo.pl/35379075#tab=click',
                                      'http://www.ceneo.pl/33379075#tab=reviews', 'http://www.ceneo.pl/33379077']
    correct_code_product_for_correct_product_addresses_list = ['35379075', '33379075', '33379077']
    incorrect_product_addresses_list = ['http://www.ceneo.pl/3539075#tab=click',
                                        'http://www.cceneo.pl/33379758#tab=reviews', 'http://www.ceneo.pl/3379077']

    correct_product_addresses_list_skapiec = ['http://www.skapiec.pl/site/cat/8/comp/14360822']

    incorrect_product_addresses_list_skapiec = ['http://www.skapiec.pl/site/cat/8/comp/143608222',
                                                'http://www.skapiec.pl/site/cat/8/comp/1436082',
                                                'http://www.sskapiec.pl/site/cat/8/comp/14360822']

    def aseration_for_single_input_text(self, input_product_list, separator, expected_product_list,
                                        expected_statistic_info, is_ceneo=True):
        for endings_begin in self.endings_list:
            for endings_end in self.endings_list:
                productText = endings_begin + separator.join(input_product_list) + endings_end
                self.assertEqual(expected_product_list, get_product_code(is_ceneo, productText, self.statistics))
                self.assertEqual(expected_statistic_info, self.statistics.getStatisticInfo())
                self.statistics.clearStatisticInfo()

    def get_error_info(self, text_lsit):
        feedback_info = ""
        for text in text_lsit:
            feedback_info += "Błąd. Nie rozpoznano produktu: " + text + "\n"
        return feedback_info

    # actual Testcase
    # Ceneo
    def test_getProductCode_one_correct_product_code_semicolon_separator(self):
        self.aseration_for_single_input_text([self.correct_product_codes_list[0]], self.separators_list[0],
                                             [self.correct_product_codes_list[0]], "")

    def test_getProductCode_list_of_correct_product_code_semicolon_separator(self):
        self.aseration_for_single_input_text(self.correct_product_codes_list, self.separators_list[0],
                                             self.correct_product_codes_list, "")

    def test_getProductCode_one_correct_product_addresses_semicolon_separator(self):
        self.aseration_for_single_input_text([self.correct_product_addresses_list[0]], self.separators_list[0],
                                             [self.correct_code_product_for_correct_product_addresses_list[0]], "")

    def test_getProductCode_list_of_correct_product_addresses_semicolon_separator(self):
        self.aseration_for_single_input_text(self.correct_product_addresses_list, self.separators_list[0],
                                             self.correct_code_product_for_correct_product_addresses_list, "")

    def test_getProductCode_one_correct_product_code_coma_separator(self):
        self.aseration_for_single_input_text([self.correct_product_codes_list[0]], self.separators_list[1],
                                             [self.correct_product_codes_list[0]], "")

    def test_getProductCode_list_of_correct_product_code_coma_separator(self):
        self.aseration_for_single_input_text(self.correct_product_codes_list, self.separators_list[1],
                                             self.correct_product_codes_list, "")

    def test_getProductCode_one_correct_product_addresses_coma_separator(self):
        self.aseration_for_single_input_text([self.correct_product_addresses_list[0]], self.separators_list[1],
                                             [self.correct_code_product_for_correct_product_addresses_list[0]], "")

    def test_getProductCode_list_of_correct_product_addresses_coma_separator(self):
        self.aseration_for_single_input_text(self.correct_product_addresses_list, self.separators_list[1],
                                             self.correct_code_product_for_correct_product_addresses_list, "")

    def test_getProductCode_one_correct_product_code_new_line_separator(self):
        self.aseration_for_single_input_text([self.correct_product_codes_list[0]], self.separators_list[2],
                                             [self.correct_product_codes_list[0]], "")

    def test_getProductCode_list_of_correct_product_code_new_line_separator(self):
        self.aseration_for_single_input_text(self.correct_product_codes_list, self.separators_list[2],
                                             self.correct_product_codes_list, "")

    def test_getProductCode_one_correct_product_addresses_new_line_separator(self):
        self.aseration_for_single_input_text([self.correct_product_addresses_list[0]], self.separators_list[2],
                                             [self.correct_code_product_for_correct_product_addresses_list[0]], "")

    def test_getProductCode_list_of_correct_product_addresses_new_line_separator(self):
        self.aseration_for_single_input_text(self.correct_product_addresses_list, self.separators_list[2],
                                             self.correct_code_product_for_correct_product_addresses_list, "")

    @unittest.skip("skipping")
    def test_getProductCode_one_incorrect_product_code_semicolon_separator(self):
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text([self.incorrect_product_codes_list[0]], self.separators_list[0], [],
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_list_of_incorrect_product_code_semicolon_separator(self):
        expected_statistic_info = self.get_error_info(self.incorrect_product_codes_list)
        self.aseration_for_single_input_text(self.incorrect_product_codes_list, self.separators_list[0], [],
                                             expected_statistic_info)

    def test_getProductCode_one_incorrect_product_addresses_semicolon_separator(self):
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text([self.incorrect_product_addresses_list[0]], self.separators_list[0], [],
                                             expected_statistic_info)

    def test_getProductCode_list_of_incorrect_product_addresses_semicolon_separator(self):
        expected_statistic_info = self.get_error_info(self.incorrect_product_addresses_list)
        self.aseration_for_single_input_text(self.incorrect_product_addresses_list, self.separators_list[0], [],
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_one_incorrect_product_code_coma_separator(self):
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text([self.incorrect_product_codes_list[0]], self.separators_list[1], [],
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_list_of_incorrect_product_code_coma_separator(self):
        expected_statistic_info = self.get_error_info(self.incorrect_product_codes_list)
        self.aseration_for_single_input_text(self.incorrect_product_codes_list, self.separators_list[1], [],
                                             expected_statistic_info)

    def test_getProductCode_one_incorrect_product_addresses_coma_separator(self):
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text([self.incorrect_product_addresses_list[0]], self.separators_list[1], [],
                                             expected_statistic_info)

    def test_getProductCode_list_of_incorrect_product_addresses_coma_separator(self):
        expected_statistic_info = self.get_error_info(self.incorrect_product_addresses_list)
        self.aseration_for_single_input_text(self.incorrect_product_addresses_list, self.separators_list[1], [],
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_one_incorrect_product_code_new_line_separator(self):
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text([self.incorrect_product_codes_list[0]], self.separators_list[2], [],
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_list_of_incorrect_product_code_new_line_separator(self):
        expected_statistic_info = self.get_error_info(self.incorrect_product_codes_list)
        self.aseration_for_single_input_text(self.incorrect_product_codes_list, self.separators_list[2], [],
                                             expected_statistic_info)

    def test_getProductCode_one_incorrect_product_addresses_new_line_separator(self):
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text([self.incorrect_product_addresses_list[0]], self.separators_list[2], [],
                                             expected_statistic_info)

    def test_getProductCode_list_of_incorrect_product_addresses_new_line_separator(self):
        expected_statistic_info = self.get_error_info(self.incorrect_product_addresses_list)
        self.aseration_for_single_input_text(self.incorrect_product_addresses_list, self.separators_list[2], [],
                                             expected_statistic_info)

    def test_getProductCode_correct_product_code_correct_product_address_correct_product_code_semicolon_separator(self):
        input_product_list = [self.correct_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.correct_product_codes_list[1]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[1]]
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list, "")

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_correct_product_address_correct_product_code_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.correct_product_codes_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_correct_product_code_incorrect_product_address_correct_product_code_semicolon_separator(
            self):
        input_product_list = [self.correct_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.correct_product_codes_list[1]]
        expected_product_list = [self.correct_product_codes_list[0], self.correct_product_codes_list[1]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_code_correct_product_address_incorrect_product_code_semicolon_separator(
            self):
        input_product_list = [self.correct_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.incorrect_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_incorrect_product_address_correct_product_code_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.correct_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_code_incorrect_product_address_incorrect_product_code_semicolon_separator(
            self):
        input_product_list = [self.correct_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.incorrect_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_correct_product_address_incorrect_product_code_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.incorrect_product_codes_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_codes_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_incorrect_product_address_incorrect_product_code_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.incorrect_product_codes_list[1]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                 self.incorrect_product_codes_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], [], expected_statistic_info)

    def test_getProductCode_correct_product_code_correct_product_address_correct_product_code_coma_separator(self):
        input_product_list = [self.correct_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.correct_product_codes_list[1]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[1]]
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list, "")

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_correct_product_address_correct_product_code_coma_separator(self):
        input_product_list = [self.incorrect_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.correct_product_codes_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_correct_product_code_incorrect_product_address_correct_product_code_coma_separator(self):
        input_product_list = [self.correct_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.correct_product_codes_list[1]]
        expected_product_list = [self.correct_product_codes_list[0], self.correct_product_codes_list[1]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_code_correct_product_address_incorrect_product_code_coma_separator(self):
        input_product_list = [self.correct_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.incorrect_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_incorrect_product_address_correct_product_code_coma_separator(self):
        input_product_list = [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.correct_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_code_incorrect_product_address_incorrect_product_code_coma_separator(self):
        input_product_list = [self.correct_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.incorrect_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_correct_product_address_incorrect_product_code_coma_separator(self):
        input_product_list = [self.incorrect_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.incorrect_product_codes_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_codes_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_incorrect_product_address_incorrect_product_code_coma_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.incorrect_product_codes_list[1]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                 self.incorrect_product_codes_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], [], expected_statistic_info)

    def test_getProductCode_correct_product_code_correct_product_address_correct_product_code_new_line_separator(self):
        input_product_list = [self.correct_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.correct_product_codes_list[1]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[1]]
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list, "")

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_correct_product_address_correct_product_code_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.correct_product_codes_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_correct_product_code_incorrect_product_address_correct_product_code_new_line_separator(
            self):
        input_product_list = [self.correct_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.correct_product_codes_list[1]]
        expected_product_list = [self.correct_product_codes_list[0], self.correct_product_codes_list[1]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_code_correct_product_address_incorrect_product_code_new_line_separator(
            self):
        input_product_list = [self.correct_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.incorrect_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_incorrect_product_address_correct_product_code_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.correct_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_code_incorrect_product_address_incorrect_product_code_new_line_separator(
            self):
        input_product_list = [self.correct_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.incorrect_product_codes_list[0]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_correct_product_address_incorrect_product_code_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.correct_product_addresses_list[0],
                              self.incorrect_product_codes_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_codes_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_code_incorrect_product_address_incorrect_product_code_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                              self.incorrect_product_codes_list[1]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0],
                 self.incorrect_product_codes_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], [], expected_statistic_info)

    def test_getProductCode_correct_product_address_correct_product_code_correct_product_address_semicolon_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.correct_product_addresses_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[1]]
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list, "")

    def test_getProductCode_incorrect_product_address_correct_product_code_correct_product_address_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.correct_product_addresses_list[0]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_address_incorrect_product_code_correct_product_address_semicolon_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.correct_product_addresses_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[1]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_correct_product_address_correct_product_code_incorrect_product_address_semicolon_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_address_incorrect_product_code_correct_product_address_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.correct_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_address_incorrect_product_code_incorrect_product_address_semicolon_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_incorrect_product_address_correct_product_code_incorrect_product_address_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.incorrect_product_addresses_list[1]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_addresses_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_address_incorrect_product_code_incorrect_product_address_semicolon_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                 self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[0], [], expected_statistic_info)

    def test_getProductCode_correct_product_address_correct_product_code_correct_product_address_coma_separator(self):
        input_product_list = [self.correct_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.correct_product_addresses_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[1]]
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list, "")

    def test_getProductCode_incorrect_product_address_correct_product_code_correct_product_address_coma_separator(self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.correct_product_addresses_list[0]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_address_incorrect_product_code_correct_product_address_coma_separator(self):
        input_product_list = [self.correct_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.correct_product_addresses_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[1]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_correct_product_address_correct_product_code_incorrect_product_address_coma_separator(self):
        input_product_list = [self.correct_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_address_incorrect_product_code_correct_product_address_coma_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.correct_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_address_incorrect_product_code_incorrect_product_address_coma_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_incorrect_product_address_correct_product_code_incorrect_product_address_coma_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.incorrect_product_addresses_list[1]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_addresses_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_address_incorrect_product_code_incorrect_product_address_coma_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                 self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[1], [], expected_statistic_info)

    def test_getProductCode_correct_product_address_correct_product_code_correct_product_address_new_line_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.correct_product_addresses_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[1]]
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list, "")

    def test_getProductCode_incorrect_product_address_correct_product_code_correct_product_address_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.correct_product_addresses_list[0]]
        expected_product_list = [self.correct_product_codes_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_address_incorrect_product_code_correct_product_address_new_line_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.correct_product_addresses_list[1]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_code_product_for_correct_product_addresses_list[1]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_correct_product_address_correct_product_code_incorrect_product_address_new_line_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0],
                                 self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info([self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_address_incorrect_product_code_correct_product_address_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.correct_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_correct_product_address_incorrect_product_code_incorrect_product_address_new_line_separator(
            self):
        input_product_list = [self.correct_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_product_list = [self.correct_code_product_for_correct_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_codes_list[0], self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    def test_getProductCode_incorrect_product_address_correct_product_code_incorrect_product_address_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.correct_product_codes_list[0],
                              self.incorrect_product_addresses_list[1]]
        expected_product_list = [self.correct_product_codes_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_addresses_list[1]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], expected_product_list,
                                             expected_statistic_info)

    @unittest.skip("skipping")
    def test_getProductCode_incorrect_product_address_incorrect_product_code_incorrect_product_address_new_line_separator(
            self):
        input_product_list = [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                              self.incorrect_product_addresses_list[0]]
        expected_statistic_info = self.get_error_info(
                [self.incorrect_product_addresses_list[0], self.incorrect_product_codes_list[0],
                 self.incorrect_product_addresses_list[0]])
        self.aseration_for_single_input_text(input_product_list, self.separators_list[2], [], expected_statistic_info)
