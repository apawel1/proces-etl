class Statistics:
    # Obsolete
    statisticinfo = ""

    def addStatisticInfo(self, info):
        self.statisticinfo = self.statisticinfo + info + "\n"

    def clearStatisticInfo(self):
        self.statisticinfo = ""

    def getStatisticInfo(self):
        return self.statisticinfo

    # new statistics
    products_processed_extract = None
    review_processed_extract = None
    products_not_found_extract = None
    url_open_extract = None
    time_extract = None

    products_processed_transform = None
    review_processed_transform = None
    time_transform = None

    products_updated_load = None
    products_new_load = None
    time_load = None

    def clearStatistic(self):
        self.products_processed_extract = None
        self.review_processed_extract = None
        self.products_not_found_extract = None
        self.time_extract = None
        self.url_open_extract = None

        self.products_processed_transform = None
        self.review_processed_transform = None
        self.time_transform = None

        self.products_updated_load = None
        self.products_new_load = None
        self.time_load = None

    def getStatistic(self):
        statistic = ""
        if self.products_processed_extract is not None or self.review_processed_extract is not None or self.products_not_found_extract is not None or self.time_extract is not None:
            statistic += "Extract:"
            statistic += "\n"
            statistic += self.statisticinfo
            if self.url_open_extract is not None:
                statistic += "Ilość pobranych stron:"
                statistic += str(self.url_open_extract)
                statistic += "\n"
            if self.products_processed_extract is not None:
                statistic += "Przetworzone produkty:"
                statistic += str(self.products_processed_extract)
                statistic += "\n"
            if self.review_processed_extract is not None:
                statistic += "Przetworzone opinie:"
                statistic += str(self.review_processed_extract)
                statistic += "\n"
            if self.products_not_found_extract is not None:
                statistic += "Nie znaleziono produktów:"
                statistic += str(self.products_not_found_extract)
                statistic += "\n"
            if self.time_extract is not None:
                statistic += "Czas wykonania Operacji:"
                statistic += str(self.time_extract[:6])
                statistic += "s\n"
            statistic += "\n"

        if self.products_processed_transform is not None or self.review_processed_transform is not None or self.time_transform is not None:
            statistic += "Transform:"
            statistic += "\n"
            if self.products_processed_transform is not None:
                statistic += "Przetworzone produkty:"
                statistic += str(self.products_processed_transform)
                statistic += "\n"
            if self.review_processed_transform is not None:
                statistic += "Przetworzone opinie:"
                statistic += str(self.review_processed_transform)
                statistic += "\n"
            if self.time_transform is not None:
                statistic += "Czas wykonania Operacji:"
                statistic += str(self.time_transform[:6])
                statistic += "s\n"
            statistic += "\n"

        if self.products_updated_load is not None or self.products_new_load is not None or self.time_load is not None:
            statistic += "Load:"
            statistic += "\n"
            if self.products_updated_load is not None:
                statistic += "Liczba zaktualizowanych produktów wraz z opiniami:"
                statistic += str(self.products_updated_load)
                statistic += "\n"
            if self.products_new_load is not None:
                statistic += "Liczba dodancych produktów wraz z opiniami:"
                statistic += str(self.products_new_load)
                statistic += "\n"
            if self.time_load is not None:
                statistic += "Czas wykonania Operacji:"
                statistic += str(self.time_load[:6])
                statistic += "s\n"
            statistic += "\n"

        return statistic

    def increase_url_open_extract(self):
        if self.url_open_extract is None:
            self.url_open_extract = 1
        else:
            self.url_open_extract += 1

    def increase_products_processed_extract(self):
        if self.products_processed_extract is None:
            self.products_processed_extract = 1
        else:
            self.products_processed_extract += 1

    def increase_review_processed_extract(self):
        if self.review_processed_extract is None:
            self.review_processed_extract = 1
        else:
            self.review_processed_extract += 1

    def increase_not_found_extract(self, product_code):
        if self.products_not_found_extract is None:
            self.products_not_found_extract = product_code
        else:
            self.products_not_found_extract += ", " + product_code

    def set_time_extract(self, time):
        self.time_transform = time

    def increase_products_processed_transform(self):
        if self.products_processed_transform is None:
            self.products_processed_transform = 1
        else:
            self.products_processed_transform += 1

    def increase_review_processed_transform(self):
        if self.review_processed_transform is None:
            self.review_processed_transform = 1
        else:
            self.review_processed_transform += 1

    def set_time_transform(self, time):
        self.time_transform = time

    def increase_products_updated_load(self):
        if self.products_updated_load is None:
            self.products_updated_load = 1
        else:
            self.products_updated_load += 1

    def increase_products_new_load(self):
        if self.products_new_load is None:
            self.products_new_load = 1
        else:
            self.products_new_load += 1

    def set_time_load(self, time):
        self.time_load = time
