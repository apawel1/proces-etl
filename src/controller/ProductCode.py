import re


def set_separators_based_on_content(text):
    if text.replace(' ', '').isdigit():
        return r'[ ;,\n]\s*'
    return r'[;,\n]\s*'


def get_product_code(is_ceneo, product_text, statistics):
    product_code_list = []
    separators = set_separators_based_on_content(product_text)
    products_string_list = re.split(separators, product_text)

    for product_string in products_string_list:
        product_string.strip()
        if not product_string:
            continue

        if is_ceneo and re.match("^[0-9]{8}\Z",product_string): #ceneo product code
            product_code_list.append(product_string)
        elif not is_ceneo and re.match("^[0-9]{6}\Z", product_string): #skapiec product code
            product_code_list.append(product_string)
        else:
            if is_ceneo:
                product_code = re.search('http://www.ceneo.pl/([0-9]{8})', product_string) #ceneo adress
            else:
                product_code = re.search('http://www.skapiec.pl(\/.{1,6}\/.{1,5}\/.{1,3}\/.{1,6}\/[0-9]{8})', product_string) #ceneo adress
            if product_code:
                product_code_list.append(product_code.group(1))
            elif not re.match('http://', product_string) or re.match('https://', product_string):
                product_code_list.append(product_string) #nazwa produktu
            else:
                statistics.addStatisticInfo("Błąd. Nie rozpoznano produktu: " + product_string)
    return product_code_list
