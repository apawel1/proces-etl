from unittest import TestCase
from src.controller.HtmlParser import HtmlParser


class TestHtmlParser(TestCase):
    def test_search_output_from_ceneo_check_whole_dict(self):
        html_parser = HtmlParser("playstation")
        product_code = '40789576'
        product_name = 'Sony PlayStation 4 1TB Czarna + Star Wars: Battlefront'
        dict_with_products = html_parser.get_search_output_or_product()
        self.assertTrue(product_code in dict_with_products.keys())
        self.assertTrue(product_name in dict_with_products.values())
        self.assertTrue(dict_with_products[product_code] == product_name)

    def test_search_output_from_skapiec_first(self):
        html_parser = HtmlParser("playstation", True)
        product_code = '/site/cat/56/comp/7182805'
        product_name = 'Sony PlayStation 4 500 GB'
        dict_with_products = html_parser.get_search_output_or_product()
        self.assertTrue(product_code in dict_with_products.keys())
        self.assertTrue(product_name in dict_with_products.values())
        self.assertTrue(dict_with_products[product_code] == product_name)

    def test_get_product_from_ceneo(self):
        html_parser = HtmlParser("24493469")
        product = html_parser.get_search_output_or_product()
        self.assertEqual("24493469", product.product_code_ceneo)
        self.assertEqual("", product.product_code_skapiec)
        self.assertEqual("Sony PlayStation 4", product.type_of_device)
        self.assertEqual("Sony", product.product_brand)
        self.assertEqual("Sony PlayStation 4 500GB", product.model_name)
        self.assertEqual("", product.extra_information)
        self.assertEqual("Ceneo", product.review_list[0].service_name)

        self.assertTrue("mało gier" in product.review_list[0].defects)
        self.assertTrue("płatne opcje sieciowe" in product.review_list[0].defects)

        self.assertTrue("dobrze leżący w dłoni pad" in product.review_list[0].advantages)
        self.assertTrue("świetna grafika" in product.review_list[0].advantages)

        expected_content_of_first_review = """Na ciekawe gry niestety trzeba jeszcze poczekać, a za płatne opcje sieciowe dostajemy jedną darmową grę miesięcznie od PSN. Niestety, jak dotąd raczej są to słabiutkie tytuły. Jeśli chodzi o samą konsolę, bez większych zarzutów."""
        self.assertEqual(expected_content_of_first_review, product.review_list[0].content_of_review)
        self.assertEqual("5/5", product.review_list[0].number_of_stars)
        self.assertEqual("pavel", product.review_list[0].review_author)
        self.assertEqual("2014-05-02 13:27:19", product.review_list[0].review_date)
        self.assertEqual("Polecam", product.review_list[0].is_recommend)
        self.assertEqual("145", product.review_list[0].number_of_positive_votes)
        self.assertEqual("232", product.review_list[0].number_of_all_votes)
        self.assertEqual(293, len(product.review_list))

    def test_get_product_from_ceneo_second_try(self):
        html_parser = HtmlParser("35379075")
        product = html_parser.get_search_output_or_product()
        self.assertEqual("35379075", product.product_code_ceneo)
        self.assertEqual("", product.product_code_skapiec)
        self.assertEqual("Dyski SSD", product.type_of_device)
        self.assertEqual("Samsung", product.product_brand)
        self.assertEqual("Samsung SSD Evo 850 250GB (MZ-75E250B/EU)", product.model_name)
        self.assertEqual("", product.extra_information)
        self.assertEqual("Ceneo", product.review_list[0].service_name)
        self.assertEqual(72, len(product.review_list))

    def test_get_product_from_skapiec(self):
        html_parser = HtmlParser("/site/cat/56/comp/11823662", is_skapiec=True)
        product = html_parser.get_search_output_or_product()
        self.assertEqual("", product.product_code_ceneo)
        self.assertEqual("/site/cat/56/comp/11823662", product.product_code_skapiec)
        self.assertEqual("Konsole Sony", product.type_of_device)
        self.assertEqual("Sony", product.product_brand)
        self.assertEqual("Sony PlayStation 4 1TB", product.model_name)
        self.assertEqual("", product.extra_information)
        for review in product.review_list:
            self.assertEqual("Skapiec", review.service_name)
        self.assertTrue("glosny" in product.review_list[0].defects)
        adventages = ['cena', 'wyglad', 'cena jakosc', 'jakosc', 'funkcjonalnosc']
        self.assertEqual(adventages, product.review_list[0].advantages)
        expected_content_of_first_review = """Chyba nie trzeba opisywac Konsola najnowszej generacji ktora jeszcze przez co najmniej 3 lata bedzie zapewniac rozrywke na najwyzszym poziomie"""
        self.assertEqual(expected_content_of_first_review, product.review_list[0].content_of_review)
        self.assertEqual("5/5", product.review_list[0].number_of_stars)
        self.assertEqual("4/5", product.review_list[6].number_of_stars)
        self.assertEqual("Konrad", product.review_list[0].review_author)
        self.assertEqual("31 października 2015", product.review_list[0].review_date)
        self.assertEqual("", product.review_list[0].is_recommend)
        self.assertEqual("0", product.review_list[0].number_of_positive_votes)
        self.assertEqual("0", product.review_list[0].number_of_all_votes)
        #
        # self.assertEqual(292, len(product.review_list))


    def test_get_product_from_skapiec_more_reviews(self):
        html_parser = HtmlParser("/site/cat/56/comp/7182805", is_skapiec=True)
        product = html_parser.get_search_output_or_product()
        self.assertEqual("", product.product_code_ceneo)
        self.assertEqual("/site/cat/56/comp/7182805", product.product_code_skapiec)
        self.assertEqual("Konsole Sony", product.type_of_device)
        self.assertEqual("Sony", product.product_brand)
        self.assertEqual("Sony PlayStation 4 500 GB", product.model_name)
        self.assertEqual("", product.extra_information)
        for review in product.review_list:
            self.assertEqual("Skapiec", review.service_name)
        defects = ['cena', 'drogie gry', 'grzeje sie']
        self.assertEqual(defects, product.review_list[27].defects)
        adventages = ['wygląd', 'wydajność', 'funkcjonalność']
        self.assertEqual(adventages, product.review_list[0].advantages)
        expected_content_of_first_review = """Konsola naprawde dobra wspaniala grafika przejrzyste MENU wygodny kontroler i wiele swietnych gier Polecam"""
        self.assertEqual(expected_content_of_first_review, product.review_list[27].content_of_review)
        self.assertEqual("5/5", product.review_list[0].number_of_stars)
        self.assertEqual("Tomek", product.review_list[0].review_author)
        self.assertEqual("21 grudnia 2013", product.review_list[0].review_date)
        self.assertEqual("", product.review_list[0].is_recommend)
        self.assertEqual("19", product.review_list[0].number_of_positive_votes)
        self.assertEqual("22", product.review_list[0].number_of_all_votes)
        self.assertEqual(182, len(product.review_list))

