from unittest import TestCase
from src.controller.HtmlGetter import SourceFromWebsite

class TestSourceFromWebsite(TestCase):

	def test_is_skapiec_true_url_should_be_from_skapiec(self):
		self.assertEqual('http://www.skapiec.net/search/netsprint/0/1/0/?q=test', SourceFromWebsite("test", True).url)

	def test_is_skapiec_false_product_code_is_number_url_ceneo_product_code(self):
		self.assertEqual('http://www.ceneo.pl/789456', SourceFromWebsite("789456", False).url)

	def test_is_skapiec_false_product_code_is_number_url_ceneo_product_name(self):
		self.assertEqual('http://www.ceneo.pl/;szukaj-samsung', SourceFromWebsite("samsung", False).url)
	pass
