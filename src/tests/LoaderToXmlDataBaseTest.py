from unittest import TestCase
from src.controller.TransformToXml import TransformToXml
from src.model.Product import Product
from src.model.Review import Review
from src.controller.LoaderToXmlDataBase import LoaderToXmlDataBase, DB_DIR
import os


class TestTest(TestCase):
    def setUp(self):
        transformToXml = TransformToXml()

        review_one = Review(["powolny", "mała pojemność"], ["cena", "wyglad"], "Polecam", 4, "Killer", "12.12.2015",
                            "True", 1, 2, "Ceneo")
        review_two = Review(["powolny"], ["cena"], "Polecam", 3, "Nowak", "06.12.2015", "True", 0, 1, "Ceneo")
        review_three = Review(["powolny", "mała pojemność"], [], "Zdecydowanie nie polecam", 1, "Killer", "12.12.2015",
                              "False", 0, 0, "Ceneo")
        review_four = Review(["bardzo powolny", "bardzo mała pojemność"], [], "Nie polecam", 1, "Killero", "12.18.2015",
                             "False", 6, 10, "Skapiec")

        product_one = Product("26380453", "", "Dysk SSD", "ADATA", "S102 PRO 256GB", "Super szybki dysk SSD",
                              [review_one, review_two])
        product_two = Product("36380453", "/site/cat/56/comp/718280", "Dysk HDD", "WD", "2TB", "Dysk HDD",
                              [review_three, review_four])

        two_products = [product_one, product_two]

        self.xml_list_holder = transformToXml.transform(two_products, self.Dummy_increase_progress,
                                                        self.Dummy_progress_completed,
                                                        self.Dummy_increase_products_processed_transform,
                                                        self.Dummy_increase_review_processed_transform)

    def tearDown(self):
        # try:
        #     os.stat(DB_DIR)
        #     file_list = os.listdir(DB_DIR)
        #     for file in file_list:
        #         os.remove(os.path.join(DB_DIR, file))
        #     os.removedirs(DB_DIR)
        # except:
        pass

    def Dummy_increase_progress(self, dumytext, steep=1):
        pass

    def Dummy_progress_completed(self):
        pass

    def Dummy_increase_products_processed_transform(self):
        pass

    def Dummy_increase_review_processed_transform(self):
        pass

    def test_load_two_product_create_one_products_xml_and_separate_file_for_review_per_product_and_service(self):
        products_string = open(os.path.join('LoaderToXmlDataBaseTestXml', 'Products.xml')).read()

        Ceneo_26380453_product = open(os.path.join("LoaderToXmlDataBaseTestXml", '26380453.xml')).read()

        Ceneo_36380453_product = open(os.path.join("LoaderToXmlDataBaseTestXml", '36380453.xml')).read()

        Skapiec__site_cat_56_comp718280_product = open(
                os.path.join("LoaderToXmlDataBaseTestXml", '_site_cat_56_comp_718280.xml')).read()

        loader_to_xml_data_base = LoaderToXmlDataBase()
        loader_to_xml_data_base.load(self.xml_list_holder, self.Dummy_increase_progress, self.Dummy_progress_completed,
                                     self.Dummy_increase_products_processed_transform,
                                     self.Dummy_increase_review_processed_transform)

        output_26380453_product = open(os.path.join(DB_DIR, '26380453.xml')).read()
        output_36380453_product = open(os.path.join(DB_DIR, '36380453.xml')).read()
        output_363804_product = open(os.path.join(DB_DIR, '_site_cat_56_comp_718280.xml')).read()
        output_product_string = open(os.path.join(DB_DIR, 'Products.xml')).read()

        self.assertEqual(products_string, output_product_string)
        self.assertEqual(Ceneo_26380453_product, output_26380453_product)
        self.assertEqual(Ceneo_36380453_product, output_36380453_product)
        self.assertEqual(Skapiec__site_cat_56_comp718280_product, output_363804_product)
