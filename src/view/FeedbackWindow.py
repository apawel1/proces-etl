# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'FeedbackWindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_FeedbackWindow(object):
    def setupUi(self, FeedbackWindow):
        FeedbackWindow.setObjectName(_fromUtf8("FeedbackWindow"))
        FeedbackWindow.resize(400, 246)
        self.verticalLayoutWidget = QtGui.QWidget(FeedbackWindow)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 371, 221))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.feedbackTextBrowser = QtGui.QTextBrowser(self.verticalLayoutWidget)
        self.feedbackTextBrowser.setAutoFillBackground(True)
        self.feedbackTextBrowser.setObjectName(_fromUtf8("feedbackTextBrowser"))
        self.verticalLayout.addWidget(self.feedbackTextBrowser)
        self.closeButton = QtGui.QPushButton(self.verticalLayoutWidget)
        self.closeButton.setObjectName(_fromUtf8("closeButton"))
        self.verticalLayout.addWidget(self.closeButton)

        self.retranslateUi(FeedbackWindow)
        QtCore.QMetaObject.connectSlotsByName(FeedbackWindow)

    def retranslateUi(self, FeedbackWindow):
        FeedbackWindow.setWindowTitle(_translate("FeedbackWindow", "FeedbackWindows", None))
        self.feedbackTextBrowser.setHtml(_translate("FeedbackWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None))
        self.closeButton.setText(_translate("FeedbackWindow", "Zamknij", None))

