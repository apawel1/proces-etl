from bs4 import BeautifulSoup
from src.controller.HtmlGetter import SourceFromWebsite
import re
from collections import OrderedDict
from src.model.Product import Product
from src.model.Review import Review


class HtmlParser:
    def __init__(self, product, is_skapiec=False, is_both_services=False,
                 feedback_info=None, get_select_product=None, progress_bar=None, statistics=None):
        self.is_both_services = is_both_services
        self.feedback_info = feedback_info
        self.getSelectProduct = get_select_product
        self.progress_bar = progress_bar
        self.statistics = statistics
        self.product_code = product
        self.progress_bar[0](1)
        self.html_getter = SourceFromWebsite(product, is_skapiec=is_skapiec,
                                             statistic_url_open=self.statistics[0])

    def get_search_output_or_product(self):
        if not self.html_getter.is_name:
            return self.__create_product()
        else:
            return self.__get_search_output()

    def __add_review_from_ceneo(self, product):
        if self.is_both_services:
            self.product_code = product.model_name
            self.html_getter.is_skapiec = False
            self.html_getter.url = self.html_getter.set_url_based_on_product(self.product_code)
            search_output = self.__get_search_output()
            if not search_output:
                self.feedback_info("Nie znaleziono wyszukiwanego produktu w serwisie Ceneo")
                return
            elif len(search_output) == 1:
                key, value = search_output.popitem()
                self.product_code = str(key)
            else:
                self.product_code = self.getSelectProduct(search_output,
                                                          "Wybierz najlepiej pasujący produkt z serwisu Ceneo")
            if self.product_code is None:
                return
            self.html_getter.url = self.html_getter.set_url_based_on_product(self.product_code)
            product.product_code_ceneo = self.product_code
            html_code = self.html_getter.get_html_code()
            soup = BeautifulSoup(html_code, "html.parser")
            product.review_list.extend(self.__get_review_list_ceneo(soup))

    def __add_review_from_skapiec(self, product):
        if self.is_both_services:
            self.html_getter.is_skapiec = True
            self.product_code = product.model_name
            self.html_getter.url = self.html_getter.set_url_based_on_product(self.product_code)
            search_output = self.__get_search_output()
            if not search_output:
                self.feedback_info("Nie znaleziono wyszukiwanego produktu w serwisie Skapiec")
                return
            elif len(search_output) == 1:
                key, value = search_output.popitem()
                self.product_code = str(key)
            else:
                self.product_code = self.getSelectProduct(search_output,
                                                                     "Wybierz najlepiej pasujący produkt z serwisu Skapiec")
            if self.product_code is None:
                return
            self.html_getter.url = self.html_getter.set_url_based_on_product(self.product_code)
            product.product_code_skapiec = self.product_code
            html_code = self.html_getter.get_html_code()
            soup = BeautifulSoup(html_code, "html.parser")
            product.review_list.extend(self.__get_review_list_skapiec(soup))

    def __create_product(self):
        self.html_code = self.html_getter.get_html_code()
        if self.html_getter.is_skapiec:
            product = self.__create_product_from_skapiec()
            self.progress_bar[2]()
            self.__add_review_from_ceneo(product)
        else:
            product = self.__create_product_from_ceneo()
            self.progress_bar[2]()
            self.__add_review_from_skapiec(product)
        self.progress_bar[2]()
        return product

    def __create_product_from_skapiec(self):
        soup = BeautifulSoup(self.html_code, "html.parser")
        product_code_ceneo = ''
        product_code_skapiec = self.product_code
        type_of_device = self.__set_value_or_empty_string(
                soup.find_all("span", {"typeof": "v:Breadcrumb"})[-1])
        product_brand = self.__get_product_band_skapiec(soup)
        model_name = self.__set_value_or_empty_string(
                soup.find("h1", {"itemprop": "name"}))
        extra_information = ""
        review_list = self.__get_review_list_skapiec(soup, model_name)
        return Product(product_code_ceneo, product_code_skapiec, type_of_device, product_brand,
                       model_name, extra_information, review_list)
        pass

    def __create_product_from_ceneo(self):
        soup = BeautifulSoup(self.html_code, "html.parser")
        product_code_ceneo = self.product_code
        product_code_skapiec = ''
        type_of_device = self.__set_value_or_empty_string(
                soup.find_all("span", {"itemprop": "title"})[-1])
        product_brand = self.__get_product_band_ceneo(soup)
        model_name = self.__set_value_or_empty_string(
                soup.find("h1", {"class": "product-name"}))
        extra_information = self.__set_value_or_empty_string(
                soup.find("div", {"class": "ProductSublineTags"}))
        review_list = self.__get_review_list_ceneo(soup, model_name)
        return Product(product_code_ceneo, product_code_skapiec, type_of_device, product_brand,
                       model_name, extra_information, review_list)

    @staticmethod
    def __set_value_or_empty_string(value):
        if value is not None:
            return value.text.strip()
        else:
            return ""

    @staticmethod
    def __get_product_band_ceneo(soup):
        product_brand = ""
        product_brand_tmp = soup.find("div", {"class": "specs-group"})
        if product_brand_tmp:
            if HtmlParser.__set_value_or_empty_string(product_brand_tmp.find('th')).lower() == "producent":
                product_brand = HtmlParser.__set_value_or_empty_string(
                        product_brand_tmp.find("li", {"class": "attr-value"}))
            else:
                regex = '"Producent\s*:\s*(.*)?",'
                list_of_matches = re.findall(re.compile(regex), HtmlParser.__set_value_or_empty_string(soup))
                if len(list_of_matches):
                    product_brand = list_of_matches[0]
        return product_brand

    @staticmethod
    def __get_product_band_skapiec(soup):
        product_brand = ""
        regex = 'Producent\s(.*)'
        list_of_matches = re.findall(re.compile(regex), HtmlParser.__set_value_or_empty_string(soup))
        if len(list_of_matches):
            product_brand = list_of_matches[0]
        return product_brand

    def __get_review_list_ceneo(self, soup, model_name=''):
        reviews = []
        tmp_num_of_reviews = HtmlParser.__set_value_or_empty_string(soup.find("span", {"itemprop": "reviewCount"}))
        num_of_reviews = int(tmp_num_of_reviews) if tmp_num_of_reviews else 0
        page_numbers = int(num_of_reviews / 10)
        if num_of_reviews % 10:
            page_numbers += 1
        self.progress_bar[0](page_numbers+1)
        html_reviews = soup.find_all("li", {"class": "product-review js_product-review"})
        for html_review in html_reviews:
            reviews.append(self.__create_review_based_on_html_ceneo(html_review))
        self.progress_bar[1]("Przetworzono strone z opiniami dla produktu : " + model_name + " z Ceneo")
        for i in range(2, page_numbers+1):
            self.html_code = SourceFromWebsite(
                    url="http://www.ceneo.pl/"+self.product_code+"/opinie-" + str(i),
                    statistic_url_open=self.statistics[0]).get_html_code()
            soup = BeautifulSoup(self.html_code, "html.parser")
            html_reviews = soup.find_all("li", {"class": "product-review js_product-review"})
            for html_review in html_reviews:
                reviews.append(self.__create_review_based_on_html_ceneo(html_review))
            self.progress_bar[1]("Przetworzono " + str(i) +
                                 " strone z opiniami dla produktu : " +
                                 model_name + " z Ceneo")
        return reviews

    def __get_review_list_skapiec(self, soup, model_name=''):
        reviews = []
        num_of_reviews = int(HtmlParser.__get_number_of_from_html_string(soup.find('a', {"class": "stars-container"})))
        if not num_of_reviews:
            return reviews
        page_numbers = int(num_of_reviews / 30)
        if num_of_reviews % 30:
            page_numbers += 1
        self.progress_bar[0](page_numbers+1)
        html_reviews = soup.find_all("div", {"class": "opinion-wrapper"})
        comments_zone = soup.find_all("div", {"class": "comments-zone"})
        for html_review, comment_zone in zip(html_reviews, comments_zone):
            reviews.append(self.__create_review_based_on_html_skapiec(html_review, comment_zone))
        self.progress_bar[1]("Przetworzono strone z opiniami dla produktu : " + model_name + " z Skapiec")
        for i in range(2, page_numbers+1):
            self.html_code = SourceFromWebsite(
                    url="http://www.skapiec.pl" + self.product_code + "_komentarze/" + str(i),
                    statistic_url_open=self.statistics[0]).get_html_code()
            soup = BeautifulSoup(self.html_code, "html.parser")
            html_reviews = soup.find_all("div", {"class": "opinion-wrapper"})
            comments_zone = soup.find_all("div", {"class": "comments-zone"})
            for html_review, comment_zone in zip(html_reviews, comments_zone):
                reviews.append(self.__create_review_based_on_html_skapiec(html_review, comment_zone))
            self.progress_bar[1]("Przetworzono " + str(i) +
                                 " strone z opiniami dla produktu : " +
                                 model_name + " z Skapiec")
        return reviews

    @staticmethod
    def __create_review_based_on_html_skapiec(html_review, comment_zone):
        defects = []
        advantages = []
        pros_and_cons = html_review.find("ul", {"class": "pros-n-cons"})
        if pros_and_cons:
            pros_and_cons = pros_and_cons.find_all('li')
            if pros_and_cons[-1].find(text="Wady"):
                defects = HtmlParser.__set_value_or_empty_string(pros_and_cons[-1]).replace("Wady ", "", 1).split(', ')
            if pros_and_cons[0].find(text="Zalety"):
                advantages = HtmlParser.__set_value_or_empty_string(pros_and_cons[0]).replace("Zalety ", "", 1).split(', ')
        content_of_review = HtmlParser.__set_value_or_empty_string(html_review.find_all('p')[0])
        number_of_stars = HtmlParser.__get_number_of_stars_from_percent_skapiec(
                html_review.find('span', {'class': 'stars m'}).get('style'))
        review_author = HtmlParser.__set_value_or_empty_string(html_review.find('span', {'class': 'author'}))
        review_date = HtmlParser.__set_value_or_empty_string(html_review.find('span', {'class': 'date'}))
        is_recommend = ''
        votes_tmp = comment_zone.find("span", {"class": "btn gray link"})
        number_of_positive_votes = HtmlParser.__get_number_of_positive_votes(votes_tmp)
        number_of_all_votes = str(int(HtmlParser.__get_number_of_from_html_string(
                votes_tmp.findNext("span", {"class": "btn gray link"}))) + int(number_of_positive_votes))
        service_name = "Skapiec"
        return Review(defects, advantages, content_of_review,
                      number_of_stars, review_author, review_date,
                      is_recommend, number_of_positive_votes, number_of_all_votes, service_name)
        pass

    @staticmethod
    def __get_number_of_positive_votes(positive_votes_html):
        number_of_positive_votes = HtmlParser.__set_value_or_empty_string(positive_votes_html)
        return re.findall(r'\d+', number_of_positive_votes)[0]

    @staticmethod
    def __get_number_of_from_html_string(positive_votes_html):
        number_of_negative_votes = HtmlParser.__set_value_or_empty_string(positive_votes_html)
        if not number_of_negative_votes:
            return 0
        return re.findall(r'\d+', number_of_negative_votes)[0]

    @staticmethod
    def __get_number_of_stars_from_percent_skapiec(string_with_width_percent):
        percent = int((re.findall(r'\d+', string_with_width_percent))[0])
        return str(int(percent/20)) + '/5'

    @staticmethod
    def __create_review_based_on_html_ceneo(html_review):
        defects = []
        for defect in html_review.find("span", {"class": "cons-cell"}).find_all("li"):
            defects.append(HtmlParser.__set_value_or_empty_string(defect))
        advantages = []
        for advantage in html_review.find("span", {"class": "pros-cell"}).find_all("li"):
            advantages.append(HtmlParser.__set_value_or_empty_string(advantage))
        content_of_review = HtmlParser.__set_value_or_empty_string(
                html_review.find("p", {"class": "product-review-body"}))
        number_of_stars = HtmlParser.__set_value_or_empty_string(
                html_review.find("span", {"class": "review-score-count"}))
        review_author = HtmlParser.__set_value_or_empty_string(
                html_review.find("div", {"class": "product-reviewer"}))
        review_date = html_review.find("span", {"class": "review-time"}).find("time").get("datetime")
        is_recommend = HtmlParser.__set_value_or_empty_string(
                html_review.find("div", {"class": "product-review-summary"}))
        number_of_positive_votes = HtmlParser.__set_value_or_empty_string(
                html_review.find("span", {"id": re.compile("votes-yes-\d+")}))
        number_of_all_votes = HtmlParser.__set_value_or_empty_string(
                html_review.find("span", {"id": re.compile("votes-\d+")}))
        service_name = "Ceneo"
        return Review(defects, advantages, content_of_review,
                      number_of_stars, review_author, review_date,
                      is_recommend, number_of_positive_votes, number_of_all_votes, service_name)
        pass

    @staticmethod
    def __is_recommend_ceneo(html_review):
        if not html_review.find("em", {"class": "product-recommended"}):
            pass

    def __get_search_output(self):
        self.html_code = self.html_getter.get_html_code()
        if self.html_getter.is_skapiec:
            return self.__parse_search_output_from_skapiec()
        else:
            return self.__parse_search_output_from_ceneo()

    def __parse_search_output_from_skapiec(self):
        products_with_code_dict = OrderedDict()
        soup = BeautifulSoup(self.html_code, "html.parser")
        products = soup.find_all("a", {"class": "redirect"})
        if products is not None:
            for product in products:
                url = product.get("href")
                if "/site/red/" not in url:
                    products_with_code_dict[product.get('href')] = product.get("title")
        if not products_with_code_dict:
            self.statistics[1](self.product_code + " - Skapiec")
        return products_with_code_dict

    def __parse_search_output_from_ceneo(self):
        products_with_code_dict = OrderedDict()
        soup = BeautifulSoup(self.html_code, "html.parser")
        data = soup.find_all("div", {"class": "category-list-body js_category-list-body js_search-results"})[0]
        products = data.find_all("a", {"class": " js_conv"})
        if products is not None:
            for product in products:
                products_with_code_dict[self.__get_product_number_from_url(product.get('href'))]\
                    = HtmlParser.__set_value_or_empty_string(product)
        if not products_with_code_dict:
            self.statistics[1](self.product_code + " - Ceneo")
        return products_with_code_dict

    @staticmethod
    def __get_product_number_from_url(url_text):
        pattern = re.compile(r'(\d+)#')
        return ''.join(re.findall(pattern, url_text))
