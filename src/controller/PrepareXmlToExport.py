import xml.etree.ElementTree as ET
from src.controller.LoaderToXmlDataBase import DB_DIR
import os


def prepare_xml_to_export():
    try:
        tree_product = ET.parse(os.path.join(DB_DIR, 'Products.xml'))
        root_product = tree_product.getroot()
        for it, product in enumerate(root_product):
            product_name_ceneo = product.get('id_ceneo')
            product_name_skapiec = product.get('id_skapiec')
            product_name_skapiec = product_name_skapiec.replace("/", "_")
            product_name_ceneo += ".xml"
            product_name_skapiec += ".xml"
            number_of_ceneo_review = 0
            try:
                tree = ET.parse(os.path.join(DB_DIR, product_name_ceneo))
                root = tree.getroot()
                number_of_ceneo_review = len(root)
                for review in root:
                    product.append(review)

            except:
                pass

            try:
                tree = ET.parse(os.path.join(DB_DIR, product_name_skapiec))
                root = tree.getroot()
                for review in root:
                    review.set('id', (number_of_ceneo_review + int(review.get('id'))).__str__())
                    product.append(review)
            except:
                pass

        return tree_product
    except:
        pass


def prepare_xmls_to_export_to_csv():
    review_list = []

    try:
        tree_product = ET.parse(os.path.join(DB_DIR, 'Products.xml'))
        root_product = tree_product.getroot()
        for it, product in enumerate(root_product):
            add_parameter('id_ceneo', product.get('id_ceneo'), product)
            add_parameter('id_skapiec', product.get('id_skapiec'), product)
            product_name_ceneo = product.get('id_ceneo')
            product_name_ceneo += ".xml"
            product_name_skapiec = product.get('id_skapiec')
            product_name_skapiec = product_name_skapiec.replace("/", "_")
            product_name_skapiec += ".xml"
            review_list.append(product_name_ceneo)
            review_list.append(product_name_skapiec)

        root_reviews = ET.Element('ETL_Review')
        for review_file_name in review_list:
            try:
                tree_review_tmp = ET.parse(os.path.join(DB_DIR, review_file_name))
                root_review_tmp = tree_review_tmp.getroot()
                for it, review in enumerate(root_review_tmp):
                    add_parameter('code', review_file_name[:(len(review_file_name)-4)], review)
                    root_reviews.append(review)
            except:
                pass
        return [tree_product, root_reviews]
    except:
        pass


def add_parameter(parameter_name, parameter_value, parent):
    parameter = ET.Element(parameter_name)
    parent.append(parameter)
    parameter.text = parameter_value
