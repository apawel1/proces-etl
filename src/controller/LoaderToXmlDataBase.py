import xml.etree.ElementTree as ET
import os

DB_DIR = 'DB'


class LoaderToXmlDataBase:
    def load(self, xml_holder, increase_progress, progress_completed, increase_products_updated_load, increase_products_new_load):
        try:
            os.stat(DB_DIR)
        except:
            os.mkdir(DB_DIR)
        self.save_products(xml_holder[-1], increase_progress, increase_products_updated_load, increase_products_new_load)
        for i, product in enumerate(xml_holder[-1]):
            increase_progress("Load: Zapisywanie opinii produktu " + (i + 1).__str__() + "/" + product.__len__().__str__())
            product_code_ceneo = product.get('id_ceneo')
            product_code_skapiec = product.get('id_skapiec')
            if product_code_ceneo != "":
                tree_ceneo = ET.ElementTree(xml_holder[i * 2])
                ceneo_tree = tree_ceneo
                try:
                    ceneo_tree.write(os.path.join(DB_DIR, product_code_ceneo + ".xml"), encoding="unicode", xml_declaration=True)
                except:
                    ceneo_tree.write(os.path.join(DB_DIR, product_code_ceneo + ".xml"), encoding="utf-8", xml_declaration=True)
            if product_code_skapiec != "":
                tree_skapiec = ET.ElementTree(xml_holder[i * 2 + 1])
                skapiec_tree = tree_skapiec
                product_code_skapiec = product_code_skapiec.replace("/", "_")
                try:
                    skapiec_tree.write(os.path.join(DB_DIR, product_code_skapiec + ".xml"), encoding="unicode", xml_declaration=True)
                except:
                    skapiec_tree.write(os.path.join(DB_DIR, product_code_skapiec + ".xml"), encoding="utf-8", xml_declaration=True)
        progress_completed()

    def save_products(self, xml_product, increase_progress, increase_products_updated_load, increase_products_new_load):
        try:
            os.stat(os.path.join(DB_DIR, "Products.xml"))
            # print("file exist")
            tree = ET.parse(os.path.join(DB_DIR, "Products.xml"))
            root = tree.getroot()
            for i, child_xml in enumerate(xml_product):
                increase_progress("Load: Przetważamoe  produktu " + (i + 1).__str__() + "/" + xml_product.__len__().__str__())
                child_xml_ceneo = child_xml.get('id_ceneo')
                child_xml_skapiec = child_xml.get('id_skapiec')
                if child_xml_ceneo !="":
                    if self.is_present(child_xml_ceneo, root):
                        # print(child_xml_ceneo, "found")
                        increase_products_updated_load()
                        if child_xml_skapiec != "":
                            child_r.set('id_skapiec', child_xml_skapiec)
                        continue
                if child_xml_skapiec !="":
                    if self.is_present(child_xml_skapiec, root):
                        # print(child_xml_skapiec, "found")
                        increase_products_updated_load()
                        if child_xml_ceneo != "":
                            child_r.set('id_skapiec', child_xml_ceneo)
                        continue
                found = False
                for child_r in root:
                    if child_xml.find('model_name').text == child_r.find('model_name').text:
                        found = True
                        increase_products_updated_load()
                        if child_r.get('id_ceneo') == "":
                            child_r.set('id_ceneo', child_xml_ceneo)
                            break
                        if child_r.get('id_skapiec') == "":
                            child_r.set('id_skapiec', child_xml_skapiec)
                            break
                if found:
                    continue
                root.append(child_xml)
                increase_products_new_load()
            try:
                tree.write(os.path.join(DB_DIR, "Products.xml"), encoding="unicode", xml_declaration=True)
            except:
                tree.write(os.path.join(DB_DIR, "Products.xml"), encoding="utf-8", xml_declaration=True)

        except:
            tree = ET.ElementTree(xml_product)
            try:
                tree.write(os.path.join(DB_DIR, "Products.xml"), encoding="unicode", xml_declaration=True)
            except:
                tree.write(os.path.join(DB_DIR, "Products.xml"), encoding="utf-8", xml_declaration=True)

    def is_present(self, code, root):
        for child_r in root:
            child_r_ceneo = child_r.get('id_ceneo')
            child_r_skapiec = child_r.get('id_skapiec')
            if child_r_ceneo == code or child_r_skapiec == code:
                return True
        return False
