
class Product:
    def __init__(self, product_code_ceneo, product_code_skapiec, type_of_device, product_brand, model_name, extra_information, review_list):
        self.product_code_ceneo = product_code_ceneo
        self.product_code_skapiec = product_code_skapiec
        self.type_of_device = type_of_device
        self.product_brand = product_brand
        self.model_name = model_name
        self.extra_information = extra_information
        self.review_list = review_list
