# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ShowDb.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ShowDb(object):
    def setupUi(self, ShowDb):
        ShowDb.setObjectName(_fromUtf8("ShowDb"))
        ShowDb.resize(780, 600)
        ShowDb.setMinimumSize(QtCore.QSize(780, 600))
        ShowDb.setMaximumSize(QtCore.QSize(780, 600))
        self.label = QtGui.QLabel(ShowDb)
        self.label.setGeometry(QtCore.QRect(-170, 40, 580, 13))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayoutWidget = QtGui.QWidget(ShowDb)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(30, 10, 704, 581))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setSizeConstraint(QtGui.QLayout.SetMinAndMaxSize)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.textFilterInput = QtGui.QTextEdit(self.verticalLayoutWidget)
        self.textFilterInput.setMaximumSize(QtCore.QSize(16777215, 25))
        self.textFilterInput.setObjectName(_fromUtf8("textFilterInput"))
        self.verticalLayout.addWidget(self.textFilterInput)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(-1, 2, -1, -1)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.tableWidget = QtGui.QTableWidget(self.verticalLayoutWidget)
        self.tableWidget.setMinimumSize(QtCore.QSize(700, 500))
        self.tableWidget.setMaximumSize(QtCore.QSize(700, 500))
        self.tableWidget.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        self.tableWidget.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        self.tableWidget.setTextElideMode(QtCore.Qt.ElideMiddle)
        self.tableWidget.setObjectName(_fromUtf8("tableWidget"))
        self.tableWidget.setColumnCount(6)
        self.tableWidget.setRowCount(3)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setVerticalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(2, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(3, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(4, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(5, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(1, 0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(1, 1, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(2, 0, item)
        item = QtGui.QTableWidgetItem()
        self.tableWidget.setItem(2, 1, item)
        self.horizontalLayout.addWidget(self.tableWidget)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(ShowDb)
        QtCore.QMetaObject.connectSlotsByName(ShowDb)

    def retranslateUi(self, ShowDb):
        ShowDb.setWindowTitle(_translate("ShowDb", "Baza Danych", None))
        self.label.setText(_translate("ShowDb", "Wyszukaj:", None))
        self.label_2.setText(_translate("ShowDb", "Wyszukaj:", None))
        self.tableWidget.setSortingEnabled(True)
        item = self.tableWidget.verticalHeaderItem(0)
        item.setText(_translate("ShowDb", "1", None))
        item = self.tableWidget.verticalHeaderItem(1)
        item.setText(_translate("ShowDb", "2", None))
        item = self.tableWidget.verticalHeaderItem(2)
        item.setText(_translate("ShowDb", "3", None))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("ShowDb", "ID Ceneo", None))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("ShowDb", "ID skapiec", None))
        item = self.tableWidget.horizontalHeaderItem(2)
        item.setText(_translate("ShowDb", "Rodzaj", None))
        item = self.tableWidget.horizontalHeaderItem(3)
        item.setText(_translate("ShowDb", "Marka", None))
        item = self.tableWidget.horizontalHeaderItem(4)
        item.setText(_translate("ShowDb", "Model", None))
        item = self.tableWidget.horizontalHeaderItem(5)
        item.setText(_translate("ShowDb", "Dodatkowe informacje", None))
        __sortingEnabled = self.tableWidget.isSortingEnabled()
        self.tableWidget.setSortingEnabled(False)
        item = self.tableWidget.item(1, 0)
        item.setText(_translate("ShowDb", "aaaa", None))
        item = self.tableWidget.item(1, 1)
        item.setText(_translate("ShowDb", "cccc", None))
        item = self.tableWidget.item(2, 0)
        item.setText(_translate("ShowDb", "bbbb", None))
        item = self.tableWidget.item(2, 1)
        item.setText(_translate("ShowDb", "aaaa", None))
        self.tableWidget.setSortingEnabled(__sortingEnabled)

