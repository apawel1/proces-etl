
class Review:

    def __init__(self, defects=[], advantages=[], content_of_review="", number_of_stars=-1,
                 review_author="not specified", review_date="not specified", is_recommend="",
                 number_of_positive_votes=-1, number_of_all_votes=-1, service_name="Ceneo"):
        self.defects = defects
        self.advantages = advantages
        self.content_of_review = content_of_review
        self.number_of_stars = number_of_stars
        self.review_author = review_author
        self.review_date = review_date
        self.is_recommend = is_recommend
        self.number_of_positive_votes = number_of_positive_votes
        self.number_of_all_votes = number_of_all_votes
        self.service_name = service_name
