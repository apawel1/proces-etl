import urllib.request


class SourceFromWebsite:

    URL_CENEO = 'http://www.ceneo.pl/'
    URL_CENEO_SEARCH = 'http://www.ceneo.pl/;szukaj-'
    URL_SKAPIEC = 'http://www.skapiec.pl/'
    URL_SKAPIEC_SEARCH = 'http://www.skapiec.pl/szukaj/w_calym_serwisie/'

    def __init__(self, product="", is_skapiec=False, url='', statistic_url_open=None):
        self.is_skapiec = is_skapiec
        self.statistic_url_open = statistic_url_open
        self.url = url
        self.is_name = False
        if not self.url:
            self.url = self.set_url_based_on_product(product)

    @staticmethod
    def remove_illegal_character_from_url(text):
            char_to_remove = ['!', '#', '$', '&', '(', ')', '*', '+', '/', ':', ';', '=', '?', '@', '[', ']']
            for c in char_to_remove:
                if c in text:
                    text = text.replace(c, '')
            return text.lower().replace(' ', '+')

    @staticmethod
    def change_polish_char_to_supported_char(text, dict_with_char_to_change):
        text = SourceFromWebsite.remove_illegal_character_from_url(text)
        for key, value in dict_with_char_to_change.items():
            if key.upper() in text or key.lower() in text:
                text = text.replace(key, value)
        return text

    def set_url_based_on_product(self, product):
        product = str(product)
        if self.is_skapiec:
            if product.split('/')[-1].isdigit() and product.__contains__('site'):
                return self.URL_SKAPIEC + product
            self.is_name = True
            char_to_change = {'ą': 'A', 'ę': 'E', 'ś': 'S', 'ć': 'C', 'ż': "Z", 'ź': 'X', 'ł': 'L'}
            product = SourceFromWebsite.change_polish_char_to_supported_char(product, char_to_change)
            return self.URL_SKAPIEC_SEARCH + product
        if product.isdigit():
            return self.URL_CENEO + product
        self.is_name = True
        char_to_change = {'ą': 'a', 'ę': 'e', 'ś': 's', 'ć': 'c', 'ż': "z", 'ź': 'z', 'ł': 'l'}
        product = SourceFromWebsite.change_polish_char_to_supported_char(product, char_to_change)
        return self.URL_CENEO_SEARCH + product

    def get_html_code(self):
        req = urllib.request.Request(self.url, headers={
        'User-Agent': 'Chrome/47.0.2526.106'})
        self.statistic_url_open()
        resp = urllib.request.urlopen(req)
        return resp.read()


