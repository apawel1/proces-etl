from src.controller.LoaderToXmlDataBase import DB_DIR
from src.model.Product import Product
from src.model.Review import Review
import xml.etree.ElementTree as ET
import os
import re
from PyQt4 import QtGui


def _translate(context, text, disambig):
    return QtGui.QApplication.translate(context, text, disambig)


def load_product(tableWidget, product_list_from_db):
    tableWidget.clear()
    tableWidget.setRowCount(len(product_list_from_db))
    set_column_headers(tableWidget)
    for it, product in enumerate(product_list_from_db):
        tableWidget.setItem(it, 0, QtGui.QTableWidgetItem(product.product_code_ceneo))
        tableWidget.setItem(it, 1, QtGui.QTableWidgetItem(product.product_code_skapiec))
        tableWidget.setItem(it, 2, QtGui.QTableWidgetItem(product.type_of_device))
        tableWidget.setItem(it, 3, QtGui.QTableWidgetItem(product.product_brand))
        tableWidget.setItem(it, 4, QtGui.QTableWidgetItem(product.model_name))
        tableWidget.setItem(it, 5, QtGui.QTableWidgetItem(product.extra_information))


def load_product_from_db(product_list_from_db):
    try:
        tree = ET.parse(os.path.join(DB_DIR, 'Products.xml'))
        root = tree.getroot()
        for it, product in enumerate(root):
            type_of_device = ""
            product_brand = ""
            model_name = ""
            extra_information = ""
            for att in product:
                if att.tag == 'type_of_device':
                    type_of_device = att.text
                elif att.tag == 'product_brand':
                    product_brand = att.text
                elif att.tag == 'model_name':
                    model_name = att.text
                elif att.tag == 'extra_information':
                    extra_information = att.text
            product_list_from_db.append(
                    Product(product.get('id_ceneo'), product.get('id_skapiec'), type_of_device, product_brand,
                            model_name,
                            extra_information, []))
    except:
        pass


def set_column_headers(tableWidget):
    tableWidget.setHorizontalHeaderLabels(("ID Ceneo;ID skapiec;Rodzaj;Marka;Model;Dodatkowe informacje").split(";"))


def filter_product(product_list_from_db, product_list_from_db_backup, serch_text):
    product_list_from_db = []
    for product in product_list_from_db_backup:
        temp_poroduct_name = " "
        temp_poroduct_name = temp_poroduct_name.join((product.product_code_skapiec, product.product_code_ceneo,
                                                      product.type_of_device, product.product_brand,
                                                      product.model_name))
        found = True
        words = re.split(" ", serch_text)
        for word in words:
            if not re.search(word, temp_poroduct_name, flags=re.IGNORECASE):
                found = False
        if found:
            product_list_from_db.append(product)
    return product_list_from_db


def read_review_from_db(review_list_from_db, product_name, is_ceneo):
    if not is_ceneo:
        product_name = product_name.replace("/", "_")
    product_name += ".xml"
    try:
        tree = ET.parse(os.path.join(DB_DIR, product_name))
        root = tree.getroot()
        for review in root:
            defect = []
            advantage = []
            content_of_review = ""
            number_of_stars = ""
            review_author = ""
            review_date = ""
            is_recommend = ""
            number_of_positive_votes = ""
            number_of_all_votes = ""
            service_name = ""
            for att in review:
                if att.tag == 'defect':
                    defect.append(att.text)
                elif att.tag == 'advantage':
                    advantage.append(att.text)
                elif att.tag == 'content_of_review':
                    content_of_review = att.text
                elif att.tag == 'number_of_stars':
                    number_of_stars = att.text
                elif att.tag == 'review_author':
                    review_author = att.text
                elif att.tag == 'review_date':
                    review_date = att.text
                elif att.tag == 'is_recommend':
                    if att.text == "True" or att.text == "true":
                        is_recommend = "tak"
                    elif att.text == "False" or att.text == "false":
                        is_recommend = "nie"
                    is_recommend = att.text
                elif att.tag == 'number_of_positive_votes':
                    number_of_positive_votes = att.text
                elif att.tag == 'number_of_all_votes':
                    number_of_all_votes = att.text
                elif att.tag == 'service_name':
                    service_name = att.text
            review_list_from_db.append(
                    Review(defect, advantage, content_of_review, number_of_stars, review_author, review_date,
                           is_recommend, number_of_positive_votes, number_of_all_votes, service_name))
    except:
        pass


def load_review(tableWidgetCeneo, tableWidgetSkapiec, tableWidgetAll, ceneo_review_list_from_db,
                skapiec_review_list_from_db):
    tableWidgetCeneo.setRowCount(len(ceneo_review_list_from_db))
    tableWidgetSkapiec.setRowCount(len(skapiec_review_list_from_db))
    tableWidgetAll.setRowCount((len(ceneo_review_list_from_db) + len(skapiec_review_list_from_db)))

    for it, review in enumerate(ceneo_review_list_from_db):
        tableWidgetCeneo.setItem(it, 0, QtGui.QTableWidgetItem(" ".join(review.advantages)))
        tableWidgetCeneo.setItem(it, 1, QtGui.QTableWidgetItem(" ".join(review.defects)))
        tableWidgetCeneo.setItem(it, 2, QtGui.QTableWidgetItem(review.content_of_review))
        tableWidgetCeneo.setItem(it, 3, QtGui.QTableWidgetItem(review.number_of_stars))
        tableWidgetCeneo.setItem(it, 4, QtGui.QTableWidgetItem(review.is_recommend))
        tableWidgetCeneo.setItem(it, 5, QtGui.QTableWidgetItem(review.review_author))
        tableWidgetCeneo.setItem(it, 6, QtGui.QTableWidgetItem(review.review_date))
        tableWidgetCeneo.setItem(it, 7, QtGui.QTableWidgetItem(review.number_of_positive_votes))
        tableWidgetCeneo.setItem(it, 8, QtGui.QTableWidgetItem(
                (int(review.number_of_all_votes) - int(review.number_of_positive_votes))))

        tableWidgetAll.setItem(it, 0, QtGui.QTableWidgetItem(" ".join(review.advantages)))
        tableWidgetAll.setItem(it, 1, QtGui.QTableWidgetItem(" ".join(review.defects)))
        tableWidgetAll.setItem(it, 2, QtGui.QTableWidgetItem(review.content_of_review))
        tableWidgetAll.setItem(it, 3, QtGui.QTableWidgetItem(review.number_of_stars))
        tableWidgetAll.setItem(it, 4, QtGui.QTableWidgetItem(review.is_recommend))
        tableWidgetAll.setItem(it, 5, QtGui.QTableWidgetItem(review.review_author))
        tableWidgetAll.setItem(it, 6, QtGui.QTableWidgetItem(review.review_date))
        tableWidgetAll.setItem(it, 7, QtGui.QTableWidgetItem(review.number_of_positive_votes))
        tableWidgetAll.setItem(it, 8, QtGui.QTableWidgetItem(
                (int(review.number_of_all_votes) - int(review.number_of_positive_votes))))

    for it, review in enumerate(skapiec_review_list_from_db):
        tableWidgetSkapiec.setItem(it, 0, QtGui.QTableWidgetItem(" ".join(review.advantages)))
        tableWidgetSkapiec.setItem(it, 1, QtGui.QTableWidgetItem(" ".join(review.defects)))
        tableWidgetSkapiec.setItem(it, 2, QtGui.QTableWidgetItem(review.content_of_review))
        tableWidgetSkapiec.setItem(it, 3, QtGui.QTableWidgetItem(review.number_of_stars))
        tableWidgetSkapiec.setItem(it, 4, QtGui.QTableWidgetItem(review.is_recommend))
        tableWidgetSkapiec.setItem(it, 5, QtGui.QTableWidgetItem(review.review_author))
        tableWidgetSkapiec.setItem(it, 6, QtGui.QTableWidgetItem(review.review_date))
        tableWidgetSkapiec.setItem(it, 7, QtGui.QTableWidgetItem(review.number_of_positive_votes))
        tableWidgetSkapiec.setItem(it, 8, QtGui.QTableWidgetItem(
                (int(review.number_of_all_votes) - int(review.number_of_positive_votes))))

        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 0,
                               QtGui.QTableWidgetItem(" ".join(review.advantages)))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 1, QtGui.QTableWidgetItem(" ".join(review.defects)))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 2, QtGui.QTableWidgetItem(review.content_of_review))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 3, QtGui.QTableWidgetItem(review.number_of_stars))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 4, QtGui.QTableWidgetItem(review.is_recommend))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 5, QtGui.QTableWidgetItem(review.review_author))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 6, QtGui.QTableWidgetItem(review.review_date))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 7,
                               QtGui.QTableWidgetItem(review.number_of_positive_votes))
        tableWidgetAll.setItem(it + len(ceneo_review_list_from_db), 8, QtGui.QTableWidgetItem(
                (int(review.number_of_all_votes) - int(review.number_of_positive_votes))))
