from src.controller.LoaderToXmlDataBase import DB_DIR
import xml.etree.ElementTree as ET
import os


def remove_product_in_db(from_ceneo=True):
    tree = ET.parse(os.path.join(DB_DIR, 'Products.xml'))
    root = tree.getroot()
    root_final = ET.Element('ETL_Products')

    for product in root:
        product_code_ceneo = product.get('id_ceneo')
        product_code_skapiec = product.get('id_skapiec')
        if from_ceneo:
            if product_code_ceneo == "":
                root_final.append(product)
            elif product_code_skapiec != "":
                product.set('id_ceneo', "")
                root_final.append(product)
        else:
            if product_code_skapiec == "":
                root_final.append(product)
            elif product_code_ceneo != "":
                product.set('id_skapiec', "")
                root_final.append(product)

    tree = ET.ElementTree(root_final)
    tree.write(os.path.join(DB_DIR, "Products.xml"), encoding="unicode")
    if open(os.path.join(DB_DIR, "Products.xml")).read() == '<ETL_Products />':
        try:
            os.stat(DB_DIR)
            os.remove(os.path.join(DB_DIR, "Products.xml"))
            if not os.listdir(DB_DIR):
                os.removedirs(DB_DIR)
        except:
            pass

def remove_ceneo_review():
    try:
        os.stat(DB_DIR)
        file_list = os.listdir(DB_DIR)
        for file in file_list:
            if file.__len__() == 12 and file != "Products.xml":
                os.remove(os.path.join(DB_DIR, file))
        if not os.listdir(DB_DIR):
            os.removedirs(DB_DIR)
    except:
        pass

def remove_skapiec_review():
    try:
        os.stat(DB_DIR)
        file_list = os.listdir(DB_DIR)
        for file in file_list:
            if 25 <= file.__len__() <= 35:
                os.remove(os.path.join(DB_DIR, file))
        if not os.listdir(DB_DIR):
            os.removedirs(DB_DIR)
    except:
        pass

def remove_all_db():
    try:
        os.stat(DB_DIR)
        file_list = os.listdir(DB_DIR)
        for file in file_list:
            os.remove(os.path.join(DB_DIR, file))
        os.removedirs(DB_DIR)
    except:
        pass