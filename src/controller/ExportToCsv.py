from src.controller.PrepareXmlToExport import prepare_xml_to_export, prepare_xmls_to_export_to_csv
import os
from src.controller.xml2csv import xml2csv
import xml.etree.ElementTree as ET


class ExportToCSV:

    def export(self, product_file, review_file):
        roots = prepare_xmls_to_export_to_csv()
        tree_product = ET.ElementTree(roots[0])
        tree_review = ET.ElementTree(roots[1])
        try:
            roots[0].write("tmp_product.xml", encoding="unicode", xml_declaration=True)
            tree_review.write("tmp_review.xml", encoding="unicode", xml_declaration=True)
            converter = xml2csv("tmp_product.xml", product_file)
            converter.convert(tag="product_code")
            converter = xml2csv("tmp_review.xml", review_file)
            converter.convert(tag="review")
            try:
                os.remove("tmp_product.xml")
                os.remove("tmp_review.xml")
            except:
                pass
        except:
            try:
                os.remove("tmp_product.xml")
            except:
                pass
            try:
                os.remove("tmp_review.xml")
            except:
                pass

    #     if self.save_tmp_xml():
    #         inputs = "myfile.xml"
    #         output = "myfile.csv"
    #         # try:
    #         converter = xml2csv("tmp.xml", output)
    #         converter.convert(tag="product_code")
    #         os.remove("tmp.xml")
    #         # except:
    #         #     print("Bład exportu do csv")
    #         #     try:
    #         #         os.remove("tmp.xml")
    #         #     except:
    #         #         pass
    #         #     pass
    #     else:
    #         print("Bład przetwarzania Bazy Danych")
    #
    # def save_tmp_xml(self):
    #     tree = prepare_xml_to_export()
    #     if tree:
    #        # try:
    #         tree.write("tmp.xml", encoding="unicode", xml_declaration=True)
    #         return True
    #       #  except:
    #        #     return False