# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SelectProductWindow.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SelectProductWindow(object):
    def setupUi(self, SelectProductWindow):
        SelectProductWindow.setObjectName(_fromUtf8("SelectProductWindow"))
        SelectProductWindow.resize(457, 100)
        SelectProductWindow.setAutoFillBackground(True)
        SelectProductWindow.setSizeGripEnabled(True)
        self.gridLayout = QtGui.QGridLayout(SelectProductWindow)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.SelectedProductLbl = QtGui.QLabel(SelectProductWindow)
        self.SelectedProductLbl.setAlignment(QtCore.Qt.AlignCenter)
        self.SelectedProductLbl.setObjectName(_fromUtf8("SelectedProductLbl"))
        self.verticalLayout.addWidget(self.SelectedProductLbl)
        self.SelectedProductCombo = QtGui.QComboBox(SelectProductWindow)
        self.SelectedProductCombo.setMinimumSize(QtCore.QSize(50, 20))
        self.SelectedProductCombo.setObjectName(_fromUtf8("SelectedProductCombo"))
        self.verticalLayout.addWidget(self.SelectedProductCombo)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.buttonBox = QtGui.QDialogButtonBox(SelectProductWindow)
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setCenterButtons(True)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.horizontalLayout.addWidget(self.buttonBox)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)

        self.retranslateUi(SelectProductWindow)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), SelectProductWindow.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), SelectProductWindow.reject)
        QtCore.QMetaObject.connectSlotsByName(SelectProductWindow)

    def retranslateUi(self, SelectProductWindow):
        SelectProductWindow.setWindowTitle(_translate("SelectProductWindow", "Wybór Produktu", None))
        self.SelectedProductLbl.setText(_translate("SelectProductWindow", "Wybierz jeden produkt z listy znalezionych:", None))

    def getSelected(self):
        return str(self.SelectedProductCombo.currentText())